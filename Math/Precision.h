#ifndef PRECISION_H
#define PRECISION_H
#include <float.h>

#if 0	//Single precision mode, for any code that needs to be conditionally compiled.
#define SINGLE_PRECISION
typedef float real;			//Defines a real number precision.
#define REAL_MAX FLT_MAX	//defines the highest value for the real number.
#define real_sqrt sqrtf	    // defines the precision of the square root operator.
#define real_abs fabsf		//defines the precision of the absolute magnitude operator.
#define real_sin sinf		//defines the precision of the sine operator.
#define real_cos cosf		//defines the precision of the cosine operator.
#define real_exp expf		//defines the precision of the exponent operator.
#define real_pow powf		//defines the precision of the power operator.
#define real_fmod fmodf		//defines the precision of the floating point modulo operator.
#define R_PI 3.14159f		//defines precision of PI 

#else
#define DOUBLE_PRECISION
typedef double real;
#define REAL_MAX DBL_MAX
#define real_sqrt sqrt
#define real_abs fabs
#define real_sin sin
#define real_cos cos
#define real_exp exp
#define real_pow pow
#define real_fmod fmod
#define R_PI 3.14159265358979
#endif
#endif PRECISION_H
