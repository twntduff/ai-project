#pragma once
#include "Precision.h"
#include <cmath>

class Vector3
{
protected:
	real x, y, z;

public:
	Vector3(void);
	Vector3(real a, real b, real c);
	~Vector3(void);
	Vector3 operator-(const Vector3 &vector);
	Vector3 operator-(const real &scalar);
	Vector3 operator/(const real &vector);
	Vector3& operator=(const Vector3 &vector);
	Vector3 operator+=(const Vector3 &vector);
	Vector3 operator-=(const Vector3 &vector);
	real operator*(const Vector3 &vector);		//dot product
	Vector3 operator*(const real &num);			//scale
	Vector3 operator%(const Vector3 &vector);//cross product
	bool operator!=(Vector3 &vector);
	void Invert();
	real SquareMag();
	real Mag();
	void Normalize();
	void Clear();
	real Distance(Vector3& target);
	void SetX(const real a){x = a;};
	void SetY(const real b){y = b;};
	void SetZ(const real c){z = c;};
	real GetX(){return x;};
	real GetY(){return y;};
	real GetZ(){return z;};
};

