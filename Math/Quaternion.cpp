#include "Quaternion.h"


Quaternion::Quaternion(void)
{
	r = 1;
	i = 0;
	j = 0;
	k = 0;
}

Quaternion::Quaternion(real a, real b, real c, real d)
{
	r = a;
	i = b;
	j = c;
	k = d;
}

Quaternion::~Quaternion(void)
{
}

void Quaternion::operator*=(const Quaternion &multiplier)
{
	Quaternion q = *this;

	r = q.r*multiplier.r - q.i*multiplier.i -
		q.j*multiplier.j - q.k*multiplier.k;

	i = q.r*multiplier.i + q.i*multiplier.r +
		q.j*multiplier.k - q.k*multiplier.j;

	j = q.r*multiplier.j + q.j*multiplier.r +
		q.k*multiplier.i - q.i*multiplier.k;

	k = q.r*multiplier.k + q.k*multiplier.r +
		q.i*multiplier.j - q.j*multiplier.i;
}

void Quaternion::Normalize()
{
	real d = r*r+i*i+j*j+k*k;

	//check for zero length quaternion, and use the no-ratation
	//quaternion in that case
	if(d < 0)
	{
		r = 1;
		return;
	}

	d = ((real)1.0)/real_sqrt(d);
	r *= d;
	i *= d;
	j *= d;
	k *= d;
}

void Quaternion::AddScaledVector(Vector3& vector, real scale)
{
	Quaternion q(0, vector.GetX() * scale, vector.GetY() * scale, vector.GetZ() * scale);

	q *= *this;
	r += q.r * ((real)0.5);
	i += q.i * ((real)0.5);
	j += q.j * ((real)0.5);
	k += q.k * ((real)0.5);
}

void Quaternion::RotateByVector(Vector3& vector)
{
	Quaternion q(0, vector.GetX(), vector.GetY(), vector.GetZ());

	(*this) *= q;
}