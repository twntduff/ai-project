#pragma once
#include "Vector3.h"
#include <cmath>

class Quaternion
{
private:
	real r, i, j, k;
	//holds quaternian in array form
	real data[4];

public:
	Quaternion(void);
	Quaternion(real a, real b, real c, real d);
	~Quaternion(void);
	void operator *=(const Quaternion &multiplier);
	void Normalize();
	void AddScaledVector(Vector3& vector, real scale);
	void RotateByVector(Vector3& vector);
	real GetR(){return r;};
	real GetI(){return i;};
	real GetJ(){return j;};
	real GetK(){return k;};
};

