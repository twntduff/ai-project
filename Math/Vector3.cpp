#include "Vector3.h"


Vector3::Vector3(void)
{
	x = 0;
	y = 0;
	z = 0;
}

Vector3::Vector3(real a, real b, real c)
{
	x = a;
	y = b;
	z = c;
}

Vector3::~Vector3(void)
{

}

Vector3 Vector3::operator-(const Vector3 &vector)
{
	this->x -= vector.x;
	this->y -= vector.y;
	this->z -= vector.z;

	return *this;
}

Vector3 Vector3::operator-(const real &scalar)
{
	Vector3 temp;
	temp.x = this->x - scalar;
	temp.y = this->y - scalar;
	temp.z = this->z - scalar;

	return temp;
}

Vector3 Vector3::operator/(const real &scalar)
{
	this->x = this->x/scalar;
	this->y = this->y/scalar;
	this->z = this->z/scalar;

	return *this;
}

Vector3& Vector3::operator=(const Vector3 &vector)
{
	if(this != &vector)
	{
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}
	return *this;
}

Vector3 Vector3::operator+=(const Vector3 &vector)
{
	this->x += vector.x;
	this->y += vector.y;
	this->z += vector.z;
	return *this;
}

Vector3 Vector3::operator-=(const Vector3 &vector)
{
	this->x -= vector.x;
	this->y -= vector.y;
	this->z -= vector.z;
	return *this;
}

real Vector3::operator*(const Vector3 &vector)//dot product
{
	return (x*vector.x + y*vector.y + z*vector.z);
}

Vector3 Vector3::operator*(const real &num)//scale
{
	return Vector3(x*num, y*num, z*num);
}

Vector3 Vector3::operator%(const Vector3 &vector)//cross product
{
	return Vector3(y*vector.z-z*vector.y,
		z*vector.x-x*vector.z,
		x*vector.y-y*vector.x);
}

bool Vector3::operator!=(Vector3 &vector)
{
	if(this->GetX() == vector.GetX() && this->GetY() == vector.GetY() && this->GetZ() == vector.GetZ())
		return false;

	return true;
}

void Vector3::Invert()
{
	x *= -1;
	y *= -1;
	x *= -1;
}

real Vector3::SquareMag()
{
	return (x*x+y*y+z*z);
}

real Vector3::Mag()
{
	return real_sqrt(x*x+y*y+z*z);
}

void Vector3::Normalize()
{
	real temp = this->Mag();
	if(temp > 0)
	{
		x = (x/temp);
		y = (y/temp);
		z = (z/temp);
	}
}

void Vector3::Clear()
{
	if(this != 0)
	{
		x = 0;
		y = 0;
		z = 0;
	}
}

real Vector3::Distance(Vector3& target)
{
	float distance = real_sqrt(real_pow(target.GetX() - x,2) + real_pow(target.GetY() - y,2) + real_pow(target.GetZ() - z,2));
	return distance;
}