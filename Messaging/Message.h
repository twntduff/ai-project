#ifndef MESSAGE_H
#define MESSAGE_H
#include "Enums.h"

class Message
{
private:
	int To, From, TimeSent, TimeDelivered;
	MessageType MsgType;

public:
	Message(int to, int from, int timeSent, int timeDelivered, MessageType type);
	Message(void);
	~Message(void);

	//Accessor
	int GetTo(){return To;};
	int GetFrom(){return From;};
	int GetTimeSent(){return TimeSent;};
	int GetTimeDelivered(){return TimeDelivered;};
	MessageType GetMessageType(){return MsgType;};

	//Mutator
	void SetTo(int to){To = to;};
	void SetFrom(int from){From = from;};
	void SetTimeSent(int timeSent){TimeSent = timeSent;};
	void SetTimeDelivered(int timeDelivered){TimeDelivered = timeDelivered;};
	void SetMessageType(MessageType type){MsgType = type;};

	//Method
	void SendToManager();
};
#endif