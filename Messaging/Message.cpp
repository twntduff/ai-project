#include "Message.h"
#include "MessageManager.h"

Message::Message(int to, int from, int timeSent, int timeDelivered, MessageType type)
{
	To = to;
	From = from;
	TimeSent = timeSent;
	TimeDelivered = timeDelivered;
	MsgType = type;
}

Message::Message(void)
{
}


Message::~Message(void)
{
}

void Message::SendToManager()
{
	MSNGR->AddMessage(this);
}