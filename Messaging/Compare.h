#ifndef COMPARE_H
#define COMPARE_H
#include "Message.h"

class Compare
{
public:
	bool operator () (Message* a, Message* b);
};
#endif