#include "Compare.h"

bool Compare::operator()(Message* a, Message* b)
{
	int time1 = a->GetTimeSent() + a->GetTimeDelivered();
	int time2 = b->GetTimeSent() + a->GetTimeDelivered();

	return time1 > time2;
}