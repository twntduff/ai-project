#ifndef MESSAGEMANAGER_H
#define MESSAGEMANAGER_H
#include <queue>
#include "Compare.h"
#include "FiniteStateMachine.h"

using namespace std;

#define MSNGR MessageManager::Instance()

class MessageManager
{
private:
	MessageManager(void);
	priority_queue<Message*, vector<Message*>, Compare> MessageList;

public:
	~MessageManager(void);

	static MessageManager* Instance();

	//Accessor
	priority_queue<Message*, vector<Message*>, Compare> GetMessageList();

	//Method
	void AddMessage(Message* message);
	void Dispatch();
};
#endif