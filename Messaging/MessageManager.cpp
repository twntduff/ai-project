#include "MessageManager.h"
#include "EntityManager.h"
#include "Algorithm.h"

MessageManager::MessageManager(void)
{
}


MessageManager::~MessageManager(void)
{
}

MessageManager* MessageManager::Instance()
{
	static MessageManager* pInstance = new MessageManager();
	return pInstance;
}

priority_queue<Message*, vector<Message*>, Compare> MessageManager::GetMessageList()
{
	return MessageList;
}

void MessageManager::AddMessage(Message* message)
{
	MessageList.push(message);
}

void MessageManager::Dispatch()
{
	while(!MessageList.empty() && MessageList.top()->GetTimeSent()+MessageList.top()->GetTimeDelivered() < timeGetTime())
	{
		Message* message = MessageList.top();
		BaseEntity* reciever = EMNGR->GetEntityList().at(message->GetTo());
		MessageList.pop();
		//reciever update
		reciever->Update(message);
	}
}