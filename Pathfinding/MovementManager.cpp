#include "MovementManager.h"

MovementManager::MovementManager(void)
{

}

MovementManager::~MovementManager(void)
{

}

MovementManager* MovementManager::Instance()
{
	static MovementManager* pInstance = new MovementManager();
	return pInstance;
}

void MovementManager::AddAlgo(int id, Algorithm* algo)
{
	algo->SetID(id);
	MoveList.push_back(algo);
}

void MovementManager::DeleteByID(int id)
{
	for(int i = 0; i < MoveList.size(); i++)
	{
		if(MoveList[i]->GetID() == id)
		{
			delete MoveList.at(i);
			MoveList.erase(MoveList.begin() + i);
		}
	}
}

void MovementManager::RunAlgorithms(unordered_map<int, BaseEntity*> list)
{
	if(!MoveList.empty())
	{
		for(int j = 0; j < list.size(); j++)
		{
			for(int i = 0; i < MoveList.size(); i++)
			{
				int id1 = list[j]->GetID();
				int id2 = MoveList[i]->GetID();

				if(id1 == id2)
				{
					MoveList.at(i)->Update(list.at(MoveList.at(i)->GetID()), list.at(MoveList.at(i)->GetID())->GetTarget());
					gMyGameWorld->GetSphere(list.at(MoveList.at(i)->GetID())->GetID())->SetPosition(list.at(MoveList.at(i)->GetID())->GetPosition().GetX(), 
					list.at(MoveList.at(i)->GetID())->GetPosition().GetY(), 
					list.at(MoveList.at(i)->GetID())->GetPosition().GetZ());
				}
			}
		}
		/*int i = 65;

		MoveList.at(i)->Update(list->GetEntityList().at(MoveList.at(i)->GetID()), list->GetEntityList().at(MoveList.at(i)->GetID())->GetTarget());
		gMyGameWorld->GetSphere(list->GetEntityList().at(MoveList.at(i)->GetID())->GetID())->SetPosition(list->GetEntityList().at(MoveList.at(i)->GetID())->GetPosition().GetX(), 
				list->GetEntityList().at(MoveList.at(i)->GetID())->GetPosition().GetY(), 
				list->GetEntityList().at(MoveList.at(i)->GetID())->GetPosition().GetZ());*/
	}
}