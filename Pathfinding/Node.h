#pragma once
#include <vector>
#include "Vector3.h"
#include "Enums.h"

using namespace std;


class Node
{
private:
	int Id;
	Vector3 Position;
	real Heuristic, MoveCost, Influence;
	vector<Node*> Next;
	Node* Parent;
	Quadrant Zone;
	bool InClosed, InOpen;

public:
	Node(void);
	Node(int id, Vector3 _position);
	~Node(void);

	//Accessor
	real GetInfluence(){return Influence;};
	Quadrant GetZone(){return Zone;};
	Node* GetParent(){return Parent;};
	int GetID(){return Id;};
	Vector3 GetPosition(){return Position;};
	real GetHeuristic(){return Heuristic;};
	real GetMoveCost(){return MoveCost;};
	vector<Node*> GetNext(){return Next;};
	bool GetInClosed(){return InClosed;};
	bool GetInOpen(){return InOpen;};
	
	//Mutator
	void SetInfluence(real influence){Influence = influence;};
	void SetZone(Quadrant zone){Zone = zone;};
	void IsInOpen(){InOpen = true;};
	void NotInOpen(){InOpen = false;};
	void IsInClosed(){InClosed = true;};
	void NotInClosed(){InClosed = false;};
	void SetParent(Node* parent){Parent = parent;};
	void SetID(int id){Id = id;};
	void SetNodePosition(Vector3 pos){Position = pos;};
	void SetHeuristic(real h){Heuristic = h;};
	void SetMoveCost(real cost){MoveCost = cost;};
	void AddNext(Node* next){Next.push_back(next);};

	//Method
	double GetHeuristic(Node* nodeEnd);
	double GetF(){return Heuristic + MoveCost;};
};
