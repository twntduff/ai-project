#include "PathFinding.h"
#include "ColorDefines.h"
#include "MapManager.h"
#include "MyGameWorld.h"

PathFinding::PathFinding(void)
{
	InitializedStartGoal = false;
	ReachedGoal = false;
	CurrMoveCost = 0;
}

PathFinding::~PathFinding(void)
{

}

vector<Vector3> PathFinding::FindPath(Node* start, Node* goal, Vector3 target)
{
	if(!InitializedStartGoal)
	{
		if(!OpenList.empty())
		{
			for(int i = 0; i < OpenList.size(); i++)
			{
				delete OpenList[i];
			}
			OpenList.clear();
		}

		if(!VisitedList.empty())
		{
			for(int i = 0; i < VisitedList.size(); i ++)
			{
				delete VisitedList[i];
			}
			VisitedList.clear();
		}

		if(!PathList.empty())
		{
			/*for(int i = 0; i < PathList.size(); i++)
			{
				delete PathList[i];
			}*/
			PathList.clear();
		}

		SetStartAndGoal(start, goal);
		InitializedStartGoal = true;
	}

	if(InitializedStartGoal)
	{
		vector<Vector3> list;
		list = ContinuePath(target);
		return list;
	}
	
}


//keep
void PathFinding::SetStartAndGoal(Node* start, Node* goal)
{
	StartNode = start;
	StartNode->SetMoveCost(0);
	GoalNode = goal;

	OpenList.push_back(StartNode);
}

//keep
Node* PathFinding::GetNextNode()
{
	real bestF = 99999;
	real bestInfluence = -99999;
	int nodeIndex = -1;
	Node* nextNode = NULL;

	for(int i = 0; i < OpenList.size(); i++)
	{
		if(OpenList[i]->GetInfluence() > bestInfluence)
		{
			bestInfluence = OpenList[i]->GetInfluence();

			if(OpenList[i]->GetF() * bestInfluence < bestF)
			{
				bestF = OpenList[i]->GetF();
				
				nodeIndex = i;
			}
		}
	}

	if(nodeIndex >= 0)
	{
		nextNode = OpenList[nodeIndex];
		OpenList.erase(OpenList.begin()+nodeIndex);
	}

	return nextNode;
}

vector<Vector3> PathFinding::ContinuePath(Vector3 target)
{
	Node* CurrNode = NULL;

	while(!ReachedGoal)
	{
		bool found = false;

		CurrNode = GetNextNode();
		
		if(CurrNode->GetID() == GoalNode->GetID())
			ReachedGoal = true;
		else
		{
			if(!CurrNode->GetInClosed())
			{
				VisitedList.push_back(CurrNode);
				CurrNode->IsInClosed();
			}

			for(int i = 0; i < CurrNode->GetNext().size(); i++)
			{
				double distance = CurrNode->GetPosition().Distance(CurrNode->GetNext()[i]->GetPosition());

				for(int j = 0; j < VisitedList.size(); j++)
				{
					if(CurrNode->GetNext()[i]->GetID() == VisitedList[j]->GetID())
					{
						if(CurrNode->GetNext()[i]->GetF() < VisitedList[j]->GetF())
						{
							CurrNode->GetNext()[i]->SetMoveCost(CurrNode->GetMoveCost()+distance);
							VisitedList[j]->SetParent(CurrNode);
							found = true;
						}
					}
				}

				if(!found)
				{
					for(int j = 0; j < OpenList.size(); j++)
					{
						if(CurrNode->GetNext()[i]->GetID() == OpenList[j]->GetID())
						{
							if(CurrNode->GetNext()[i]->GetF() < OpenList[j]->GetF())
							{
								CurrNode->GetNext()[i]->SetMoveCost(CurrNode->GetMoveCost()+distance);
								OpenList[j]->SetParent(CurrNode);
								found = true;
							}
							
						}
					}
				}

				if(!found)
				{
					if(!CurrNode->GetNext()[i]->GetInOpen())
					{
						CurrNode->GetNext()[i]->SetParent(CurrNode);
						OpenList.push_back(CurrNode->GetNext()[i]);
						CurrNode->GetNext()[i]->IsInOpen();
					}
					CurrNode->GetNext()[i]->SetMoveCost(CurrNode->GetMoveCost()+distance);
					
				}
			}
		}
	}

	for(int i = 0; i < MAPMNGR->GetNavMap().size(); i++)
	{
		MAPMNGR->GetNavMap()[i]->NotInClosed();
		MAPMNGR->GetNavMap()[i]->NotInOpen();
	}


	bool pathFound = false;

	if(target != Vector3(0,0,0))
		PathList.push_back(target);

	PathList.push_back(GoalNode->GetPosition());
	Node* currNode = GoalNode;

	while(!pathFound)
	{
		if(currNode->GetID() == StartNode->GetID())
			pathFound = true;
		else
		{
			currNode = currNode->GetParent();
			PathList.push_back(currNode->GetPosition());
		}

	}

	return PathList;
}
