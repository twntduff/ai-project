#include "MapManager.h"
#include "MyGameWorld.h"

MapManager::MapManager(void)
{

}

MapManager::~MapManager(void)
{

}

MapManager* MapManager::Instance()
{
	static MapManager* pInstance = new MapManager();
	return pInstance;
}

void MapManager::InitializeMap()
{
	//read in from txt file
	ifstream inFile;
	inFile.open("NavigationMap.txt");

	if(inFile.fail())
		exit(1);

	int tempId;
	double tempX, tempY, tempZ;
	string str;
	vector<string> numLines;

	while(!inFile.eof())		
	{

		inFile >> tempId;
		inFile >> tempX;
		inFile >> tempY;
		inFile >> tempZ;
		getline(inFile, str);
		numLines.push_back(str);

		Vector3 position(tempX,tempY,tempZ);
		Node* tempNode = new Node(tempId, position);
		tempNode->SetZone(FindQuadrant(position));
		NavMap.push_back(tempNode);
	}

	for(int i = 0; i < numLines.size(); i++)
	{
		for(int j = 0; j < numLines[i].size(); j++)
		{
			if(numLines[i][j] != ' ')
			{
				int childId = stoi(&numLines[i][j]);

				if(childId > 9 && childId < 100)
					j++;
				else if(childId > 99)
					j += 2;

				NavMap[i]->AddNext(NavMap[childId]);
			}
		}
	}
}

vector<Vector3> MapManager::SetGoal(Node* start, Node* goal, Vector3 target)
{
	PathFinding* tempPath = new PathFinding();

	for(int i = 0; i < NavMap.size(); i++)
	{
		NavMap[i]->SetHeuristic(NavMap[i]->GetHeuristic(goal));
	}

	return tempPath->FindPath(start, goal, target);
}

int MapManager::GetNextID()
{
	if(NavMap.empty())
		return 0;
	else
		return NavMap.size();
}

Quadrant MapManager::FindQuadrant(Vector3 pos)
{
	if(pos.GetX() < 0)
	{
		if(pos.GetX() < -90)
		{
			if(pos.GetZ() > 0)
				return Q1;
			else
				return Q5;
		}
		else
		{
			if(pos.GetZ() > 0)
				return Q2;
			else
				return Q6;
		}
	}
	else
	{
		if(pos.GetX() > 90)
		{
			if(pos.GetZ() > 0)
				return Q4;
			else
				return Q8;
		}
		else
		{
			if(pos.GetZ() > 0)
				return Q3;
			else
				return Q7;
		}
	}
}

Node* MapManager::FindClosestNode(BaseEntity* entity)
{
	Node* tempNode = entity->GetCurrentNode();
	real leastDist = 999999;

	for(int i = 0; i < NavMap.size(); i++)
	{
		real distance = entity->GetPosition().Distance(NavMap[i]->GetPosition());

		if(NavMap[i]->GetZone() == entity->GetZone() &&  distance < leastDist)
		{
			leastDist = distance;
			tempNode = NavMap[i];
		}
	}

	return tempNode;
}

void MapManager::CreateNavMapNodes()
{
		if(NavMap.size() == 0)
		return;
	else
	{
		for(int i = 0; i < NavMap.size(); i++)
		{
			int id = gMyGameWorld->CreateTriangle(NavMap[i]->GetID());
			Triangle* tempTriangle = gMyGameWorld->GetTriangle(id);

			tempTriangle->SetPosition(NavMap[i]->GetPosition().GetX(), NavMap[i]->GetPosition().GetY(), NavMap[i]->GetPosition().GetZ());
			tempTriangle->SetScale(2,2,2);
		}
	}
}

void MapManager::DeleteNavMapNodes()
{
	if(NavMap.size() == 0)
		return;
	else
	{
		gMyGameWorld->DestroyTriangles();

		NavMap.clear();
		//for(int i = 0; i < NavMap.size(); i++)
		//{
		//	NavMap.erase(NavMap.begin()+i);
		//}
	}
}

void MapManager::CalculateInfluence(unordered_map<int, BaseEntity*> list)
{
	Node* currNode = NULL;

	for(int k = 0; k < NavMap.size(); k++)
	{
		NavMap[k]->SetInfluence(0);
	}

	for(int i = 0; i < NavMap.size(); i++)
	{
		currNode = NavMap[i];

		for(int j = 0; j < list.size(); j++)
		{
			if(list[j]->GetType() != Dead)
			{
				if(currNode->GetID() == list[j]->GetCurrentNodeId() )
				{
					if(list[j]->GetObjectType() == Allie)
					{
						NavMap[i]->SetInfluence(NavMap[i]->GetInfluence()+.5);

						for(int n = 0; n < NavMap[i]->GetNext().size(); n++)
						{
							NavMap[i]->GetNext()[n]->SetInfluence(NavMap[i]->GetNext()[n]->GetInfluence()+.25);

							for(int r = 0; r < NavMap[i]->GetNext()[n]->GetNext().size(); r++)
								NavMap[i]->GetNext()[n]->GetNext()[r]->SetInfluence(NavMap[i]->GetNext()[n]->GetNext()[r]->GetInfluence()+.1);
						}
					}
					if(list[j]->GetObjectType() == Fighter)
					{
						NavMap[i]->SetInfluence(NavMap[i]->GetInfluence()+2);

						for(int n = 0; n < NavMap[i]->GetNext().size(); n++)
						{
							NavMap[i]->GetNext()[n]->SetInfluence(NavMap[i]->GetNext()[n]->GetInfluence()+1);

							for(int r = 0; r < NavMap[i]->GetNext()[n]->GetNext().size(); r++)
								NavMap[i]->GetNext()[n]->GetNext()[r]->SetInfluence(NavMap[i]->GetNext()[n]->GetNext()[r]->GetInfluence()+.5);
						}
					}
					else if(list[j]->GetObjectType() == Enemy)
					{
						NavMap[i]->SetInfluence(NavMap[i]->GetInfluence()-2);

						for(int n = 0; n < NavMap[i]->GetNext().size(); n++)
						{
							NavMap[i]->GetNext()[n]->SetInfluence(NavMap[i]->GetNext()[n]->GetInfluence()-1);

							for(int r = 0; r < NavMap[i]->GetNext()[n]->GetNext().size(); r++)
								NavMap[i]->GetNext()[n]->GetNext()[r]->SetInfluence(NavMap[i]->GetNext()[n]->GetNext()[r]->GetInfluence()-.5);
						}
					}
					else if(list[j]->GetObjectType() == Food)
					{
						NavMap[i]->SetInfluence(NavMap[i]->GetInfluence()+.5);

						for(int n = 0; n < NavMap[i]->GetNext().size(); n++)
						{
							NavMap[i]->GetNext()[n]->SetInfluence(NavMap[i]->GetNext()[n]->GetInfluence()+.25);

							for(int r = 0; r < NavMap[i]->GetNext()[n]->GetNext().size(); r++)
								NavMap[i]->GetNext()[n]->GetNext()[r]->SetInfluence(NavMap[i]->GetNext()[n]->GetNext()[r]->GetInfluence()+.1);
						}
					}
				}
			}
		}
	}

	//OutputInfluence();
}

void MapManager::OutputInfluence()
{
	for(int i = 0; i < NavMap.size(); i++)
	{
		gMyGameWorld->GetTriangle(NavMap[i]->GetID())->ClearText();
		gMyGameWorld->GetTriangle(NavMap[i]->GetID())->AddText("", NavMap[i]->GetInfluence());
	}
}