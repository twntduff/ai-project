#pragma once
#include "Algorithm.h"
#include <unordered_map>

using namespace std;

#define MOVE MovementManager::Instance()

class MovementManager
{
private:
	MovementManager(void);
	vector<Algorithm*> MoveList;

public:
	~MovementManager(void);

	//Accessor
	vector<Algorithm*> GetMoveList(){return MoveList;};

	//Method
	static MovementManager* Instance();
	void AddAlgo(int id, Algorithm* algo);
	void DeleteByID(int id);
	void RunAlgorithms(unordered_map<int, BaseEntity*> list);
};

