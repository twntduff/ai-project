#include "Node.h"

Node::Node(void)
{
	MoveCost = 0;
	Heuristic = 0;
	Parent = NULL;
	InClosed = false;
	InOpen = false;
	Influence = 0;
}

Node::Node(int id, Vector3 _position)
{
	Id = id;
	Position = _position;
	Heuristic = 0;
	MoveCost = 0;
	Parent = NULL;
	InClosed = false;
	InOpen = false;
	Influence = 0;
}

Node::~Node(void)
{
}

double Node::GetHeuristic(Node* nodeEnd)
{
	/*double x = abs(this->GetPosition().GetX() - nodeEnd->GetPosition().GetX());
	double z = abs(this->GetPosition().GetZ() - nodeEnd->GetPosition().GetZ());

	return x + z;*/

	double distance = this->GetPosition().Distance(nodeEnd->GetPosition());
	return distance;
}