#pragma once
#include "Node.h"
#include "BaseEntity.h"

using namespace std;

class PathFinding
{
private:
	vector<Node*> OpenList;
	vector<Node*> VisitedList;
	vector<Vector3> PathList;

	Node* StartNode,* GoalNode;
	bool InitializedStartGoal;
	bool ReachedGoal;
	double CurrMoveCost;

public:
	PathFinding(void);
	~PathFinding(void);

	//Accessor
	vector<Node*> GetOpenList(){return OpenList;};
	vector<Node*> GetVisitedList(){return VisitedList;};
	vector<Vector3> GetPathList(){return PathList;};

	//Mutator

	//Method
	vector<Vector3> FindPath(Node* start, Node* goal, Vector3 target);
	void SetStartAndGoal(Node* start, Node* goal);
	Node* GetNextNode();
	vector<Vector3> ContinuePath(Vector3 target);
};

