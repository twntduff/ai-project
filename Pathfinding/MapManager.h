#pragma once
#include "PathFinding.h"
#include <string>
#include <fstream>
#include <unordered_map>

#define MAPMNGR MapManager::Instance()

using namespace std;

class MapManager
{
private:
	MapManager(void);
	vector<Node*> NavMap;

public:
	~MapManager(void);

	//Accessor
	vector<Vector3> GetPathList(){return GetPathList();};
	vector<Node*> GetNavMap(){return NavMap;};

	//Mutator
	void AddNode(Node* node){NavMap.push_back(node);};

	//Method
	static MapManager* Instance();
	void InitializeMap();
	vector<Vector3> SetGoal(Node* start, Node* goal, Vector3 target);
	int GetNextID();
	Quadrant FindQuadrant(Vector3 pos);
	Node* FindClosestNode(BaseEntity* entity);
	void CreateNavMapNodes();
	void DeleteNavMapNodes();
	void CalculateInfluence(unordered_map<int, BaseEntity*> list);
	void OutputInfluence();
};

