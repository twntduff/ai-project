#pragma once
#include "Algorithm.h"

class Patrol : public Algorithm
{
public:
	Patrol(int id);
	Patrol(void);
	~Patrol(void);
	void Update(BaseEntity* entity, Vector3 target);
	int GetRandNumber(int min, int max);
};

