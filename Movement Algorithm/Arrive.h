#pragma once
#include "Algorithm.h"

class Arrive : public Algorithm
{
public:
	Arrive(int id);
	Arrive(void);
	~Arrive(void);
	void Update(BaseEntity* entity, Vector3 target);
};

