#include "Arrive.h"

Arrive::Arrive(int id)
	:Algorithm(id)
{

}

Arrive::Arrive(void)
{

}

Arrive::~Arrive(void)
{

}

void Arrive::Update(BaseEntity* entity, Vector3 target)
{
	if(entity->IsMessageSent() == true)
	{
		Vector3 desiredVel = (target - entity->GetPosition())*entity->GetVelocityMax();
		desiredVel.Normalize();
		Vector3 steering = desiredVel - entity->GetVelocity();
		steering = steering * entity->GetForceMax();
		steering = steering/entity->GetMass();

		entity->SetVelocity(((entity->GetVelocity()+=steering)*entity->GetVelocityMax())*.2);
		entity->SetPosition(entity->GetPosition()+=entity->GetVelocity());

		if(entity->GetPosition().Distance(target) <= 1000)
			entity->NotSent();
	}
	else
		entity->SetVelocity(Vector3(0,0,0));


	//doesnt work
	/*Vector3 direction = target - entity->GetPosition();
	direction.Normalize();
	Vector3 desiredVel = direction * entity->GetVelocityMax();
	Vector3 appliedVel = desiredVel - entity->GetVelocity();
	appliedVel.Normalize();
	appliedVel = appliedVel * entity->GetForceMax();
	entity->SetVelocity(entity->GetVelocity() += appliedVel);
	entity->SetPosition(entity->GetPosition() += entity->GetVelocity());*/
}