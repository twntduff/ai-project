#ifndef ALGORITHM_H
#define ALGORITHM_H
#include "BaseEntity.h"
#include "Vector3.h"
#include "MyGameWorld.h"

class Algorithm
{
private:
	int ID;

public:
	Algorithm(int id);
	Algorithm(void);
	~Algorithm(void);

	//Accessor
	int GetID(){return ID;};

	//Mutator
	void SetID(int id){ID = id;};

	//Method
	virtual void Update(BaseEntity* entity, Vector3 target) = 0;
};
#endif