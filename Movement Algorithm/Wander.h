#pragma once
#include "Algorithm.h"

class Wander : public Algorithm
{
public:
	Wander(void);
	~Wander(void);
	void Update(BaseEntity* entity, Vector3 target);
};

