#include "SeekAlgo.h"

SeekAlgo::SeekAlgo(int id)
	: Algorithm(id)
{

}

SeekAlgo::SeekAlgo(void)
{

}

SeekAlgo::~SeekAlgo(void)
{

}

void SeekAlgo::Update(BaseEntity* entity, Vector3 target)
{
	Vector3 desiredVel = (target - entity->GetPosition())*entity->GetVelocityMax();
	desiredVel.Normalize();
	Vector3 steering = desiredVel - entity->GetVelocity();
	steering = steering * entity->GetForceMax();
	steering = steering/entity->GetMass();

	entity->SetVelocity((entity->GetVelocity()+=steering)*entity->GetVelocityMax());
	entity->SetPosition(entity->GetPosition()+=entity->GetVelocity());

	/*Vector3 direction = target - entity->GetPosition();
	direction.Normalize();
	Vector3 desiredVel = direction * entity->GetVelocityMax();
	Vector3 appliedVel = desiredVel - entity->GetVelocity();
	appliedVel.Normalize();
	appliedVel = appliedVel * entity->GetForceMax();
	entity->SetVelocity(entity->GetVelocity() += appliedVel);
	entity->SetPosition(entity->GetPosition() += entity->GetVelocity());*/
}
