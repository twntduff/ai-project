#pragma once
#include "Algorithm.h"

class SeekAlgo : public Algorithm
{
public:
	SeekAlgo(int id);
	SeekAlgo(void);
	~SeekAlgo(void);
	void Update(BaseEntity* entity, Vector3 target);
};