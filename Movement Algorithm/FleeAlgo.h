#pragma once
#include "Algorithm.h"

class FleeAlgo : public Algorithm
{
public:
	FleeAlgo(int id);
	FleeAlgo(void);
	~FleeAlgo(void);
	void Update(BaseEntity* entity, Vector3 target);
};

