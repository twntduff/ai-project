
#include "Patrol.h"

Patrol::Patrol(int id)
	: Algorithm(id)
{

}

Patrol::Patrol(void)
{

}

Patrol::~Patrol(void)
{

}

void Patrol::Update(BaseEntity* entity, Vector3 target)
{
	if(entity->GetMass() >= 0)
	{
		if(entity->GetPosition().Distance(target) <= 5)
		{
			real tempX = GetRandNumber(entity->GetPosition().GetX()-100, entity->GetPosition().GetX()+100);
			real tempY = GetRandNumber(entity->GetPosition().GetY()-100, entity->GetPosition().GetY()+100);
			real tempZ = GetRandNumber(entity->GetPosition().GetZ()-100, entity->GetPosition().GetZ()+100);
			Vector3 tar(tempX, tempY, tempZ);

			Vector3 direction = tar - entity->GetPosition();
			direction.Normalize();
			Vector3 desiredVel = direction * entity->GetVelocityMax();
			Vector3 appliedVel = desiredVel - entity->GetVelocity();
			appliedVel.Normalize();
			appliedVel = appliedVel * entity->GetForceMax();
			entity->SetVelocity(entity->GetVelocity() += appliedVel);
			entity->SetPosition(entity->GetPosition() += entity->GetVelocity());
			entity->SetTarget(tar);
		}

		Vector3 direction = target - entity->GetPosition();
		direction.Normalize();
		Vector3 desiredVel = direction * entity->GetVelocityMax();
		Vector3 appliedVel = desiredVel - entity->GetVelocity();
		appliedVel.Normalize();
		appliedVel = appliedVel * entity->GetForceMax();
		entity->SetVelocity(entity->GetVelocity() += appliedVel);
		entity->SetPosition(entity->GetPosition() += entity->GetVelocity());
	}
}

int Patrol::GetRandNumber(int min, int max)
{
	return (rand()%(max-min)+min);
}
