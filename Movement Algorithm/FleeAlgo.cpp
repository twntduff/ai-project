#include "FleeAlgo.h"

FleeAlgo::FleeAlgo(int id)
	: Algorithm(id)
{

}

FleeAlgo::FleeAlgo(void)
{

}

FleeAlgo::~FleeAlgo(void)
{

}

void FleeAlgo::Update(BaseEntity* entity, Vector3 target)
{
	Vector3 direction = target - entity->GetPosition();
	direction.Normalize();
	Vector3 desiredVel = direction * entity->GetVelocityMax();
	Vector3 appliedVel = desiredVel - entity->GetVelocity();
	appliedVel.Normalize();
	appliedVel = appliedVel * entity->GetForceMax();
	entity->SetVelocity(entity->GetVelocity() += appliedVel);
	entity->SetPosition(entity->GetPosition() -= entity->GetVelocity());
}