#pragma once
#include "MyGameWorld.h"
#include "Graphics/gspOut.h"
#include "ColorDefines.h"
#include "MovementManager.h"
#include "MessageManager.h"
#include "RandMath.h"
#include "MapManager.h"

// ***********************************************************************
// Credits for developing/enhancing this framework to DeVry students: 
// Nathan Harbour: the original 2D DirectX GameWorld code - 2007
// Heath Wickman: extended the framework into 3D - 2009
// Cameron Ferguson: added the "gspOut" functionality - 2009
//					 Updated Direct Input Capabilities, Improved Camera Functionality, and updated gspOut - Oct 2010
//					 Fixed Memory Leaks with squares and triangles that were not being deleted properly - Nov 2010
//					 Added capabilities for objects to change colors									- Nov 2010
//					 Updated gspOut to prevent breaking and scrolling issues when outputting text		- Feb 2011
// Jessica McClane: fixed an annoying error that occurred on exit - 2009
// Anthony Hernandez: Added XBOX 360 Controller support - Aug 2012
//					  Added built-in crosshair options
//					  Fixed the Fixs to memory leaks with Squares, Triangles, Spheres and Lines
//					  Optimized Graphics Drawing loops
//					  Updated Documentation
//
// Your name could be here!

MyGameWorld* gMyGameWorld = 0;
char message[100];

//gspOut gDebugWindow(true, "My Debug Window");

//  Anything that needs to happen once at the beginning of the program goes here.
void MyGameWorld::Initialize(void)
{
	//GSP OUTPUT DEBUG WINDOW
	//This shows an example of how to use the gspOut debugger
	//The output debugger works just like "cout" does in a console program
	//All you have to do is include the "gspOut.h" file in other areas of the programm to access the same window: #include "gspOut.h"
	//No setup or management is required to setup or destroy the window. It's all handled for you! Just include and start using
	wout << "this is a test" << endl;
	wout << "this is another line" << endl;
	wout.clear();		//this clears the output on the window
	wout << 0 << " is less than " << 15 << " tada!" << endl << endl;
	wout.clear();
	
	wout << "Welcome to my program! Here are the instructions!" << endl;

	//CAMERA
	//initialize the camera's attributes.
	//buttons: W, A, S, D, O, SPACE, SHIFT, and Right Mouse Button are used with the camera for movement.
	/*
		W: Move forward in direction you are looking
		A: Strafe left
		S: Move backward in the direction you are looking
		D: Strafe rightec
		SPACE: Move directly up in the y dirtion
		O: Move directly down in the y direction
		SHIFT: Increase movement speed
		Right Mouse Button: Hold down to change viewing diretion
	*/
	DXCI->setCameraMode(CameraModeFreeForm);				//set up a spectator, FPS camera
	DXCI->setPosition(D3DXVECTOR3(0.0f, 400.0f, -350.0f));		//set the camera's starting position
	DXCI->setTarget(D3DXVECTOR3(0.0f, 0.0f, 0.0f));			//set the current target for the camera
	DXCI->setSpeed(20.0f);									//set the camera's movement speed, thie higher the number, the faster it moves.
	DXCI->setSensitivity(1.0f);								//set the mouse sensativity. The higher the number, the more sensative it is.
	SetCrosshairType(Normal);

	SetBackground(0, 60, 0);
	SetBackgroundBlue(100.0);

	srand(time(0));

	MAPMNGR->InitializeMap();
}

//Update method:  Anything that needs to happen every time through the animation loop goes here.
void MyGameWorld::Update(float time_elapsed)
{
	//Graphics
	static double clock = 0.0f;

	clock += time_elapsed;
	gDInput->poll();
	DXCI->adjust(time_elapsed);
	sprintf_s(message, "Clock:%f\nDT:%f\n", clock, time_elapsed);
	ClearText();
	AddText(message);

	//AI
	//Messenger
	MSNGR->Dispatch();
	//FSM
	EMNGR->RunFSM();
	//Movement
	MOVE->RunAlgorithms(EMNGR->GetEntityList());
}

//Destructor: Anything that needs to happen once at the end of the program goes here.
MyGameWorld::~MyGameWorld(void)
{
	DestroyTriangles();
	DestroySquares();
	DestroySpheres();
	DestroyLines();
}

//*****************************************************
//				   KeyPress Event
//*****************************************************
void MyGameWorld::KeyPress(WPARAM keyCode)
{
	if ( keyCode == 'P' || keyCode == 'p' || keyCode == VK_LEFT )
	{
		PauseWorld();
	}
	if ( keyCode == '1' || keyCode == '!')
	{
		MAPMNGR->CreateNavMapNodes();
		EMNGR->InitializeDemo1();
	}

	if(keyCode == '2' || keyCode == '@')
	{
		MAPMNGR->CreateNavMapNodes();
		EMNGR->InitializeDemo2();
	}

	if(keyCode == '3' || keyCode == '#')
	{
		MAPMNGR->CreateNavMapNodes();
		EMNGR->InitializeDemo3();
	}

	if(keyCode == '4' || keyCode == '$')
	{
		MAPMNGR->CreateNavMapNodes();
		EMNGR->InitializePathFindingDemo1();
	}

	if(keyCode == 'y' || keyCode == 'Y')
	{
		EMNGR->CreateArmyAnt(EMNGR->GetEntityList()[0]->GetPosition(), 150);
	}

	if(keyCode == 't' || keyCode == 'T')
	{
		EMNGR->CreateWorkAnt(EMNGR->GetEntityList()[0]->GetPosition(), 150);
	}

	if(keyCode == 'u' || keyCode == 'U')
	{
		MAPMNGR->OutputInfluence();
	}
}

//*****************************************************
//                 Controller Event
//*****************************************************
void MyGameWorld::ControllerPoll(Controller current, Controller previous)
{
	//'Select(back)' makes window fullscreen

	if(current.Start && !previous.Start)
		PauseWorld();
}