#ifndef ENUMS_H
#define ENUMS_H

enum MessageType
{
	Alive,
	Seek,
	Attack1,
	Attack2,
	Eaten,
	Flee,
	Dead
};

enum Quadrant
{
	Q1,
	Q2,
	Q3,
	Q4,
	Q5,
	Q6,
	Q7,
	Q8
};

enum ObjectType
{
	Base,
	Food,
	Enemy,
	Allie,
	Fighter
};
#endif