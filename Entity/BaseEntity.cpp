#include "BaseEntity.h"

BaseEntity::BaseEntity(int id)
{
	ID = id;
	ForceMax = .1;
	VelocityMax = .99;
	TargetID = -1;
	MessageSent = false;
	TraverseNum = 0;
	CurrentNode = NULL;
}

BaseEntity::BaseEntity(void)
{

}

BaseEntity::~BaseEntity(void)
{

}

void BaseEntity::Update(Message* message)
{

}

void BaseEntity::FindZone(Vector3 position)
{
	if(position.GetX() < 0)
	{
		if(position.GetX() < -90)
		{
			if(position.GetZ() > 0)
			{
				this->SetZone(Q1);
				return;
			}
			else
			{
				this->SetZone(Q5);
				return;
			}
		}
		else
		{
			if(position.GetZ() > 0)
			{
				this->SetZone(Q2);
				return;
			}
			else
			{
				this->SetZone(Q6);
				return;
			}
		}
	}
	else
		{
			if(position.GetX() > 90)
			{
				if(position.GetZ() > 0)
				{
					this->SetZone(Q4);
					return;
				}
				else
				{
					this->SetZone(Q8);
					return;
				}
			}
			else
			{
				if(position.GetZ() > 0)
				{
					this->SetZone(Q3);
					return;
				}
				else
				{
					this->SetZone(Q7);
					return;
				}
			}
		}
}