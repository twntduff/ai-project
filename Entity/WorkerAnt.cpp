#include "WorkerAnt.h"
#include "WorkAntCreationState.h"
#include "WorkAntSeekState.h"
#include "WorkAntFleeState.h"
#include "WorkAntReleaseState.h"

WorkerAnt::WorkerAnt(int id, Vector3 position)
	: BaseEntity(id)
{
	this->SetObjectType(Allie);
	this->SetPosition(position);
	this->SetCurrentState(WACREATION);
	this->GetCurrentState()->Enter(this);
}

WorkerAnt::WorkerAnt(void)
{
}


WorkerAnt::~WorkerAnt(void)
{
}

void WorkerAnt::Update(Message* message)
{
	if(message->GetMessageType() == Dead)
	{
		FSMGNR->Transition(WARELEASE, this);
	}
	else if(message->GetMessageType() == Flee)
	{
		FSMGNR->Transition(WAFLEE, this);	
	}
	else if(message->GetMessageType() == Seek)
	{
		FSMGNR->Transition(WASEEK, this);		
	}
	else if(message->GetMessageType() == Alive)
	{
		FSMGNR->Transition(WACREATION, this);
	}
}