#pragma once
#include "BaseEntity.h"
#include "Message.h"

class EnemyDefender : public BaseEntity
{
private:


public:
	EnemyDefender(int id, Vector3 position);
	EnemyDefender(void);
	~EnemyDefender(void);

	void Update(Message* message);
};

