#ifndef AISPHERE_H
#define AISPHERE_H
#include "BaseEntity.h"

class AISphereEntity : public BaseEntity
{
private:
	

public:
	AISphereEntity(int id);
	AISphereEntity(void);
	~AISphereEntity(void);	

	//Method
	void Update(Message* message);
};
#endif