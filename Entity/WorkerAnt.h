#pragma once
#include "BaseEntity.h"

class WorkerAnt : public BaseEntity
{
private:


public:
	WorkerAnt(int id, Vector3 position);
	WorkerAnt(void);
	~WorkerAnt(void);

	//Method
	void Update(Message* message);
};

