#include "AntHill.h"
#include "AntHillCreationState.h"
#include "AntHillReleaseState.h"

AntHill::AntHill(int id, Vector3 position)
	: BaseEntity(id)
{
	this->SetObjectType(Base);
	this->SetPosition(position);
	this->SetCurrentState(AHCREATION);
	this->GetCurrentState()->Enter(this);
	StorageAmount = 0;
}

AntHill::AntHill(void)
{

}

AntHill::~AntHill(void)
{

}

void AntHill::Update(Message* message)
{
	if(message->GetMessageType() == Eaten)
	{
		this->AddToStorageAmount(25);
	}
	if(message->GetMessageType() == Dead)
	{
		FSMGNR->Transition(AHRELEASE, this);
	}
}