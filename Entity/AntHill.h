#pragma once
#include "BaseEntity.h"
#include "Message.h"

class AntHill : public BaseEntity
{
private:
	real StorageAmount;

public:
	AntHill(int id, Vector3 position);
	AntHill(void);
	~AntHill(void);

	//Accessor
	real GetStorageAmount(){return StorageAmount;};

	//Mutator
	void AddToStorageAmount(real amount){StorageAmount += amount;};

	//Method
	void Update(Message* message);
};

