#ifndef BASEENTITY_H
#define BASEENTITY_H
#include <Windows.h>
#include "Vector3.h"
#include "Enums.h"
#include "Node.h"

#pragma comment (lib, "Winmm.lib")

class BaseState;
class Message;

class BaseEntity
{
private:
	int ID, Health, TargetID, TraverseNum, CurrentNodeId;
	Vector3 Position, Velocity, Target;
	real VelocityMax, ForceMax, Mass;
	BaseState* CurrentState;
	bool MessageSent;
	MessageType Type;

	Quadrant Zone;
	Node* CurrentNode;
	ObjectType Otype;

public:	
	vector<Vector3> TraverseList;
	BaseEntity(int id);
	BaseEntity(void);
	~BaseEntity(void);

	//Accessor
	int GetCurrentNodeId(){return CurrentNodeId;};
	ObjectType GetObjectType(){return Otype;};
	Node* GetCurrentNode(){return CurrentNode;};
	Quadrant GetZone(){return Zone;};
	int GetTravNum(){return TraverseNum;};
	vector<Vector3> GetTraverseList(){return TraverseList;};
	int GetTargetId(){return TargetID;};
	int GetID(){return ID;};
	int GetHealth(){return Health;};
	real GetMass(){return Mass;};
	BaseState* GetCurrentState(){return CurrentState;};
	bool IsMessageSent(){return MessageSent;};
	MessageType GetType(){return Type;};
	Vector3 GetPosition(){return Position;};
	Vector3 GetVelocity(){return Velocity;};
	Vector3 GetTarget(){return Target;};
	real GetVelocityMax(){return VelocityMax;};
	real GetForceMax(){return ForceMax;};

	//Mutator
	void SetCurrentNodeId(int id){CurrentNodeId = id;};
	void SetObjectType(ObjectType type){Otype = type;};
	void SetCurrentNode(Node* current){CurrentNode = current;};
	void SetZone(Quadrant zone){Zone = zone;};
	void SetTravNum(int num){TraverseNum = num;};
	void SetTraverseList(vector<Vector3> traverse){TraverseList = traverse;};
	void SetTargetID(int id){TargetID = id;};
	void SetID(int id){ID = id;};
	void SetHealth(int health){Health = health;};
	void SetMass(int mass){Mass = mass;};
	void SetCurrentState(BaseState* state){CurrentState = state;};
	void IsSent(){MessageSent = true;};
	void NotSent(){MessageSent = false;};
	void SetType(MessageType mtype){Type = mtype;};
	void SetPosition(Vector3 position){Position = position;};
	void SetVelocity(Vector3 velocity){Velocity = velocity;};
	void SetTarget(Vector3 target){Target = target;};
	void SetVelocityMax(real max){VelocityMax = max;};
	void SetForceMax(real max){ForceMax = max;};

	//Method
	virtual void Update(Message* message) = 0;
	void FindZone(Vector3 position);
};
#endif