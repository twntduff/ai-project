#include "EntityManager.h"
#include "SphereCreationState.h"
#include "MyGameWorld.h"
#include "MovementManager.h"
#include "MapManager.h"
#include "MessageManager.h"

EntityManager::EntityManager(void)
{

}

EntityManager::~EntityManager(void)
{

}

EntityManager* EntityManager::Instance()
{
	static EntityManager* pInstance = new EntityManager();
	return pInstance;
}

unordered_map<int, BaseEntity*> EntityManager::GetEntityList()
{
	return EntityList;
}

int EntityManager::GetNextValidID()
{
	if(EntityList.empty())
		return 0;
	else
		return EntityList.size();
}

void EntityManager::InitializeDemo1()
{

	////////////////////////////////////////////////////F L O O R////////////////////////////////////////////////////////
	int id = gMyGameWorld->CreateSquare(0);
	Square* tempFloor = gMyGameWorld->GetSquare(id);
	tempFloor->SetPosition(0,-3,0);
	tempFloor->SetScale(170,1,170);
	tempFloor->SetColor(TEAL);
	id++;


	////////////////////////////////////////////////////////E N T I T Y//////////////////////////////////////////////////////////////////////////////
	CreateAntHill(Vector3(-150,0,155), -1);

	CreateFoodLarge(Vector3(155,0,-100), 200);
	CreateFoodLarge(Vector3(155,0,-80), 200);
	CreateEnemyDefender(Vector3(140,0,-85), 150);
	CreateEnemyDefender(Vector3(145,0,-50), 150);
	CreateEnemyDefender(Vector3(140,0,-100), 150);

	CreateFoodLarge(Vector3(155,0,25), 200);
	CreateFoodLarge(Vector3(135,0,25), 200);
	CreateEnemyDefender(Vector3(120,0,30), 150);
	CreateEnemyDefender(Vector3(142,0,10), 150);

	CreateFoodLarge(Vector3(-35,0,155), 200);
	CreateFoodLarge(Vector3(-25,0,135), 200);

	CreateEnemyDefender(Vector3(-80,0,-20), 150);
}

void EntityManager::InitializeDemo2()
{
	////////////////////////////////////////////////////F L O O R////////////////////////////////////////////////////////
	int id = gMyGameWorld->CreateSquare(0);
	Square* tempFloor = gMyGameWorld->GetSquare(id);
	tempFloor->SetPosition(0,-3,0);
	tempFloor->SetScale(170,1,170);
	tempFloor->SetColor(TEAL);
	id++;


	////////////////////////////////////////////////////////E N T I T Y//////////////////////////////////////////////////////////////////////////////
	CreateAntHill(Vector3(-150,0,155), -1);

	CreateFoodLarge(Vector3(155,0,-100), 200);
	CreateFoodLarge(Vector3(155,0,-80), 200);
	CreateEnemyDefender(Vector3(140,0,-85), 150);
	CreateEnemyDefender(Vector3(145,0,-50), 150);
	CreateEnemyDefender(Vector3(140,0,-100), 150);

	CreateFoodLarge(Vector3(-125,0,10), 200);
	CreateFoodLarge(Vector3(-135,0,20), 200);

	CreateFoodLarge(Vector3(155,0,25), 200);
	CreateFoodLarge(Vector3(135,0,25), 200);
	CreateEnemyDefender(Vector3(120,0,30), 150);
	CreateEnemyDefender(Vector3(142,0,10), 150);

	CreateFoodLarge(Vector3(-25,0,155), 200);
	CreateFoodLarge(Vector3(-25,0,135), 200);
	CreateEnemyDefender(Vector3(-35,0,155), 150);
	CreateEnemyDefender(Vector3(-35,0,135), 150);
	
	CreateEnemyDefender(Vector3(-80,0,-20), 150);
}

void EntityManager::InitializeDemo3()
{
	////////////////////////////////////////////////////F L O O R////////////////////////////////////////////////////////
	int id = gMyGameWorld->CreateSquare(0);
	Square* tempFloor = gMyGameWorld->GetSquare(id);
	tempFloor->SetPosition(0,-3,0);
	tempFloor->SetScale(170,1,170);
	tempFloor->SetColor(TEAL);
	id++;


	////////////////////////////////////////////////////////E N T I T Y//////////////////////////////////////////////////////////////////////////////
	CreateAntHill(Vector3(135,0,-100), -1);

	CreateFoodLarge(Vector3(155,0,-100), 200);
	CreateFoodLarge(Vector3(155,0,-80), 200);


	CreateFoodLarge(Vector3(-125,0,10), 200);
	CreateFoodLarge(Vector3(-135,0,20), 200);
	CreateEnemyDefender(Vector3(-110,0,10), 150);
	CreateEnemyDefender(Vector3(-125,0,-5), 150);
	CreateEnemyDefender(Vector3(-120,0,-20), 150);

	CreateFoodLarge(Vector3(155,0,25), 200);
	CreateFoodLarge(Vector3(135,0,25), 200);
	CreateEnemyDefender(Vector3(120,0,30), 150);
	CreateEnemyDefender(Vector3(142,0,10), 150);
	CreateEnemyDefender(Vector3(140,0,-40), 150);

	CreateFoodLarge(Vector3(-25,0,155), 200);
	CreateFoodLarge(Vector3(-25,0,135), 200);
	CreateEnemyDefender(Vector3(-35,0,155), 150);
	CreateEnemyDefender(Vector3(-35,0,135), 150);
}

void EntityManager::InitializePathFindingDemo1()
{
	////////////////////////////////////////////////////F L O O R////////////////////////////////////////////////////////
	int id = gMyGameWorld->CreateSquare(0);
	Square* tempFloor = gMyGameWorld->GetSquare(id);
	tempFloor->SetPosition(0,-3,0);
	tempFloor->SetScale(170,1,170);
	tempFloor->SetColor(TEAL);
	id++;

	////////////////////////////////////////////////////////E N T I T Y//////////////////////////////////////////////////////////////////////////////
	CreateAntHill(Vector3(135,0,-100), -1);

	CreateFoodLarge(Vector3(-20,0,75), 200);
}

WorkerAnt* EntityManager::CreateWorkAnt(Vector3 pos, real mass)
{
	int id = gMyGameWorld->CreateSphere(GetNextValidID());
	Sphere* tempGSphere = gMyGameWorld->GetSphere(id);

	tempGSphere->SetScale(2,2,2);
	tempGSphere->SetColor(GREEN);
	tempGSphere->SetPosition(pos.GetX(), pos.GetY(), pos.GetZ());

	WorkerAnt* tempWorkAnt = new WorkerAnt(id, pos);
	tempWorkAnt->SetMass(mass);

	EntityList.emplace(tempWorkAnt->GetID(), tempWorkAnt);

	return tempWorkAnt;
}

AISphereEntity* EntityManager::CreateAISphere(Vector3 pos, real mass)
{
	int id = gMyGameWorld->CreateSphere(GetNextValidID());
	Sphere* tempGSphere = gMyGameWorld->GetSphere(id);

	if(mass < 0)
		tempGSphere->SetScale(2,2,2);
	else
		tempGSphere->SetScale(2,2,2);

	tempGSphere->SetPosition(pos.GetX(),pos.GetY(),pos.GetZ());

	AISphereEntity* tempAISphere = new AISphereEntity(id);
	tempAISphere->SetPosition(Vector3(pos.GetX(), pos.GetY(), pos.GetZ()));
	tempAISphere->SetMass(mass);

	EntityList.emplace(tempAISphere->GetID(), tempAISphere);

	return tempAISphere;
}

FoodLarge* EntityManager::CreateFoodLarge(Vector3 pos, real mass)
{
	int id = gMyGameWorld->CreateSquare(GetNextValidID()+1);
	Square* tempGSquare = gMyGameWorld->GetSquare(id);

	tempGSquare->SetColor(PURPLE);
	tempGSquare->SetScale(5,5,5);
	tempGSquare->SetPosition(pos.GetX(), pos.GetY(), pos.GetZ());

	FoodLarge* tempFoodLarge = new FoodLarge(id-1, pos);
	tempFoodLarge->SetMass(mass);
	tempFoodLarge->FindZone(pos);

	EntityList.emplace(tempFoodLarge->GetID(), tempFoodLarge);

	return tempFoodLarge;
}

ArmyAnt* EntityManager::CreateArmyAnt(Vector3 pos, real mass)
{
	int id = gMyGameWorld->CreateSphere(GetNextValidID());
	Sphere* tempGSphere = gMyGameWorld->GetSphere(id);

	tempGSphere->SetScale(3,3,3);
	tempGSphere->SetColor(BLUE);
	tempGSphere->SetPosition(pos.GetX(), pos.GetY(), pos.GetZ());

	ArmyAnt* tempArmyAnt = new ArmyAnt(id, pos);
	tempArmyAnt->SetMass(mass);

	EntityList.emplace(tempArmyAnt->GetID(), tempArmyAnt);

	return tempArmyAnt;
}

AntHill* EntityManager::CreateAntHill(Vector3 pos, real mass)
{
	int id = gMyGameWorld->CreateSphere(GetNextValidID());
	Sphere* tempGSphere = gMyGameWorld->GetSphere(id);

	tempGSphere->SetScale(5,5,5);
	tempGSphere->SetColor(BROWN);
	tempGSphere->SetPosition(pos.GetX(), pos.GetY(), pos.GetZ());

	AntHill* tempAntHill = new AntHill(id, pos);
	tempAntHill->SetMass(mass);

	EntityList.emplace(tempAntHill->GetID(), tempAntHill);

	return tempAntHill;
}

EnemyDefender* EntityManager::CreateEnemyDefender(Vector3 pos, real mass)
{
	int id = gMyGameWorld->CreateSphere(GetNextValidID());
	Sphere* tempGSphere = gMyGameWorld->GetSphere(id);

	tempGSphere->SetScale(4,4,4);
	tempGSphere->SetColor(RED);
	tempGSphere->SetPosition(pos.GetX(), pos.GetY(), pos.GetZ());

	EnemyDefender* tempEnemyDefender = new EnemyDefender(id, pos);
	tempEnemyDefender->SetMass(mass);

	EntityList.emplace(tempEnemyDefender->GetID(), tempEnemyDefender);

	return tempEnemyDefender;
}

void EntityManager::DeleteEntityByID(int id)
{
	delete EntityList.at(id);
	EntityList.erase(id);
}

void EntityManager::DeleteEntityList()
{
	if(EntityList.empty())
		return;
	else
	{
		gMyGameWorld->DestroySquares();
		gMyGameWorld->DestroySpheres();

		for(int i = 0; i < EntityList.size(); i++)
		{
			for(int j = 0; j < MSNGR->GetMessageList().size(); j++)
			{
				MSNGR->GetMessageList().pop();
			}

			if(EntityList[i]->GetObjectType() == Allie)
			{
				MOVE->DeleteByID(MOVE->GetMoveList()[i]->GetID());
			}

			//if(EntityList[i]->GetObjectType() == Food)
			//	gMyGameWorld->RemoveSquare(EntityList[i]->GetID()+1);
			//else
			//	gMyGameWorld->RemoveSphere(EntityList[i]->GetID());

			delete EntityList[i];
			EntityList.erase(i);
		}
	}
}

void EntityManager::RunFSM()
{
	if(EntityList.empty())
		return;
	else
	{
		for(int i = 0; i < EntityList.size(); i++)
		{
			BaseEntity* temp = EntityList[i]; 
			FSMGNR->Update(temp);
		}
	}
}

Node* EntityManager::FindClosestEnemy(BaseEntity* entity)
{
	Node* CurrNode = entity->GetCurrentNode();
	real leastDist = 999999;
	bool noEnemies = true;

	for(int i = 0; i < EntityList.size(); i++)
	{
		if(EntityList[i]->GetObjectType() == Enemy && EntityList[i]->GetType() != Dead)
		{
			real distance = entity->GetPosition().Distance(EntityList[i]->GetPosition());

			if(distance < leastDist)
			{
				noEnemies = false;
				leastDist = distance;
				CurrNode = MAPMNGR->FindClosestNode(EntityList[i]);
				entity->SetTargetID(EntityList[i]->GetID());
				entity->SetTarget(EntityList[i]->GetPosition());
			}
		}
	}

	if(noEnemies == true)
	{
		CurrNode = FindHomeBase(entity);
	}

	return CurrNode;
}

Node* EntityManager::FindClosestFoodSource(BaseEntity* entity)
{
	Node* CurrNode = entity->GetCurrentNode();
	real leastDist = 999999;
	real bestInfluence = -999999;
	bool noFood = true;

	for(int i = 0; i < EntityList.size(); i++)
	{
		if(EntityList[i]->GetObjectType() == Food && EntityList[i]->GetType() != Dead)
		{
			if(EntityList[i]->GetCurrentNode()->GetInfluence() > bestInfluence)
			{
				bestInfluence = EntityList[i]->GetCurrentNode()->GetInfluence();
				real distance = entity->GetPosition().Distance(EntityList[i]->GetPosition());

				if(distance < leastDist)
				{
					noFood = false;
					leastDist = distance;
					CurrNode = MAPMNGR->FindClosestNode(EntityList[i]);
					entity->SetTargetID(EntityList[i]->GetID());
					entity->SetTarget(EntityList[i]->GetPosition());
				}
			}
		}
	}

	if(noFood == true)
	{
		CurrNode = FindHomeBase(entity);
	}

	return CurrNode;
}

Node* EntityManager::FindHomeBase(BaseEntity* entity)
{
	Node* CurrNode = entity->GetCurrentNode();
	real leastDist = 999999;

	for(int i = 0; i < EntityList.size(); i++)
	{
		if(EntityList[i]->GetObjectType() == Base && entity->GetPosition().Distance(EntityList[i]->GetPosition()) < leastDist)
		{
			CurrNode = MAPMNGR->FindClosestNode(EntityList[i]);
			entity->SetTargetID(EntityList[i]->GetID());
			entity->SetTarget(EntityList[i]->GetPosition());
		}
	}

	return CurrNode;
}