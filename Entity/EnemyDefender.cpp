#include "EnemyDefender.h"
#include "EnemyDefenderCreationState.h"
#include "EnemyDefenderReleaseState.h"
#include "EnemyDefenderAttack1.h"
#include "EnemyDefenderFarAttack.h"

EnemyDefender::EnemyDefender(int id, Vector3 position)
	: BaseEntity(id)
{
	this->SetObjectType(Enemy);
	this->SetPosition(position);
	this->SetCurrentState(EDCREATION);
	this->GetCurrentState()->Enter(this);
}

EnemyDefender::EnemyDefender(void)
{
}

EnemyDefender::~EnemyDefender(void)
{
}

void EnemyDefender::Update(Message* message)
{
	if(message->GetMessageType() == Dead)
	{
		FSMGNR->Transition(EDRELEASE, this);
	}
	else if(message->GetMessageType() == Attack1)
	{
		FSMGNR->Transition(EDATTACK1, this);		
	}
	else if (message->GetMessageType() == Attack2)
	{
		FSMGNR->Transition(EDATTACK2, this);
	}
	else if(message->GetMessageType() == Alive)
	{
		FSMGNR->Transition(EDCREATION, this);
	}
	else if(message->GetMessageType() == Eaten)
	{
		this->SetHealth(this->GetHealth()-25);
	}
}