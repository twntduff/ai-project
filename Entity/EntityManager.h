#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H
#include <unordered_map>
#include "AISphereEntity.h"
#include "FiniteStateMachine.h"
#include "WorkerAnt.h"
#include "FoodLarge.h"
#include "ArmyAnt.h"
#include "AntHill.h"
#include "EnemyDefender.h"

using namespace std;

#define EMNGR EntityManager::Instance()

class EntityManager
{
private:
	EntityManager(void);
	unordered_map<int, BaseEntity*> EntityList;

public:
	~EntityManager(void);

	//Accessor
	unordered_map<int, BaseEntity*> GetEntityList();

	//Method
	static EntityManager* Instance();
	int GetNextValidID();
	WorkerAnt* CreateWorkAnt(Vector3 pos, real mass);
	AISphereEntity* CreateAISphere(Vector3 pos, real mass);
	FoodLarge* CreateFoodLarge(Vector3 pos, real mass);
	ArmyAnt* CreateArmyAnt(Vector3 pos, real mass);
	AntHill* CreateAntHill(Vector3 pos, real mass);
	EnemyDefender* CreateEnemyDefender(Vector3 pos, real mass);
	void InitializeDemo1();
	void InitializeDemo2();
	void InitializeDemo3();
	void InitializePathFindingDemo1();
	void DeleteEntityByID(int id);
	void DeleteEntityList();
	void RunFSM();
	real GetRandNumber(int min, int max);
	Node* FindClosestEnemy(BaseEntity* entity);
	Node* FindClosestFoodSource(BaseEntity* entity);
	Node* FindHomeBase(BaseEntity* entity);
};
#endif