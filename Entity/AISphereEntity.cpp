#include "AISphereEntity.h"
#include "FiniteStateMachine.h"
#include "SphereReleaseState.h"
#include "SphereFleeState.h"
#include "SphereSeekState.h"
#include "SphereCreationState.h"

AISphereEntity::AISphereEntity(int id)
	: BaseEntity(id)
{
	this->SetCurrentState(CREATION);
	this->GetCurrentState()->Enter(this);
}

AISphereEntity::AISphereEntity(void)
{
}

AISphereEntity::~AISphereEntity(void)
{
}

void AISphereEntity::Update(Message* message)
{

	if(message->GetMessageType() == Dead)
	{
		FSMGNR->Transition(RELEASE, this);
	}
	else if(message->GetMessageType() == Flee)
	{
		FSMGNR->Transition(FLEE, this);	
	}
	else if(message->GetMessageType() == Seek)
	{
		FSMGNR->Transition(SEEK, this);		
	}
	else if(message->GetMessageType() == Alive)
	{
		FSMGNR->Transition(CREATION, this);		
	}
}