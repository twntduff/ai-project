#include "ArmyAnt.h"
#include "ArmyAntCreationState.h"
#include "ArmyAntReleaseState.h"
#include "ArmyAntFleeState.h"
#include "ArmyAntCloseAttackState.h"
#include "ArmyAntFarAttack.h"

ArmyAnt::ArmyAnt(int id, Vector3 position)
	: BaseEntity(id)
{
	this->SetObjectType(Allie);
	this->SetPosition(position);
	this->SetCurrentState(AACREATION);
	this->GetCurrentState()->Enter(this);
}

ArmyAnt::ArmyAnt(void)
{
}


ArmyAnt::~ArmyAnt(void)
{
}

void ArmyAnt::Update(Message* message)
{
	if(message->GetMessageType() == Dead)
	{
		FSMGNR->Transition(AARELEASE, this);
	}
	else if(message->GetMessageType() == Flee)
	{
		FSMGNR->Transition(AAFLEE, this);	
	}
	else if(message->GetMessageType() == Attack1)
	{
		FSMGNR->Transition(AAATTACK1, this);		
	}
	else if(message->GetMessageType() == Attack2)
	{
		FSMGNR->Transition(AAATTACK2, this);		
	}
	else if(message->GetMessageType() == Alive)
	{
		FSMGNR->Transition(AACREATION, this);
	}
}
