#include "FoodLarge.h"
#include "FoodLargeCreationState.h"
#include "FoodLargeReleaseState.h"
#include "FoodLargeEatenState.h"

FoodLarge::FoodLarge(int id, Vector3 position)
	: BaseEntity(id)
{
	this->SetObjectType(Food);
	this->SetPosition(position);
	this->SetCurrentState(FLCREATION);
	this->GetCurrentState()->Enter(this);
}

FoodLarge::FoodLarge(void)
{
}

FoodLarge::~FoodLarge(void)
{
}

void FoodLarge::Update(Message* message)
{
	if(message->GetMessageType() == Dead)
	{
		FSMGNR->Transition(FLRELEASE, this);
	}
	else if(message->GetMessageType() == Eaten)
	{
		this->SetHealth(this->GetHealth()-25);
	}
	else if(message->GetMessageType() == Alive)
	{
		//this->SetCurrentState(WACREATION);
		FSMGNR->Transition(FLCREATION, this);
		//this->GetCurrentState()->Enter(this);
	}
}