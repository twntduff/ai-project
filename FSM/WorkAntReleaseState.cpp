#include "WorkAntReleaseState.h"


WorkAntReleaseState::WorkAntReleaseState(void)
{
}


WorkAntReleaseState::~WorkAntReleaseState(void)
{
}

void WorkAntReleaseState::Enter(BaseEntity* entity)
{

}

void WorkAntReleaseState::Execute(BaseEntity* entity)
{

}

void WorkAntReleaseState::Exit(BaseEntity* entity)
{

}

WorkAntReleaseState* WorkAntReleaseState::Instance()
{
	static WorkAntReleaseState* pInstance = new WorkAntReleaseState();
	return pInstance;
}