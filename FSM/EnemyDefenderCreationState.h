#pragma once
#include "BaseState.h"
#include "Message.h"

#define EDCREATION EnemyDefenderCreationState::Instance()

class EnemyDefenderCreationState : public BaseState
{
private:
	EnemyDefenderCreationState(void);

public:
	~EnemyDefenderCreationState(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static EnemyDefenderCreationState* Instance();
};

