#pragma once
#include "BaseState.h"
#include "Message.h"

#define EDRELEASE EnemyDefenderReleaseState::Instance()

class EnemyDefenderReleaseState : public BaseState
{
private:
	EnemyDefenderReleaseState(void);

public:
	~EnemyDefenderReleaseState(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static EnemyDefenderReleaseState* Instance();
};

