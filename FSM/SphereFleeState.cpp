#include "SphereFleeState.h"
#include "Algorithm.h"
#include "SeekAlgo.h"
#include "Arrive.h"
#include "FleeAlgo.h"
#include "Patrol.h"
#include "MovementManager.h"

SphereFleeState::SphereFleeState(void)
{

}

SphereFleeState::~SphereFleeState(void)
{

}

void SphereFleeState::Enter(BaseEntity* entity)
{
	FleeAlgo* tempAlgo = new FleeAlgo(entity->GetID());
	MOVE->AddAlgo(tempAlgo->GetID(), tempAlgo);
	entity->NotSent();
	entity->SetType(Flee);
	gMyGameWorld->GetSphere(entity->GetID())->SetColor(ORANGE);
	gMyGameWorld->GetSphere(entity->GetID())->AddText("Flee");
}

void SphereFleeState::Execute(BaseEntity* entity)
{
	if(entity->GetTargetId() != entity->GetID())
	{
		entity->SetTarget(EMNGR->GetEntityList().at(entity->GetTargetId())->GetPosition());
	}

	if(entity->GetHealth() <= 0 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Dead);
		tempMessage->SendToManager();
		entity->IsSent();
	}
	else if(entity->GetPosition().Distance(entity->GetTarget()) >= 150 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 1000,Alive);
		tempMessage->SendToManager();
		entity->IsSent();
	}
}

void SphereFleeState::Exit(BaseEntity* entity)
{
	MOVE->DeleteByID(entity->GetID());
	gMyGameWorld->ClearSphereText(entity->GetID());
}

SphereFleeState* SphereFleeState::Instance()
{
	static SphereFleeState* pInstance = new SphereFleeState();
	return pInstance;
}