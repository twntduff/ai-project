#ifndef SPHERESEEKSTATE_H
#define SPHERESEEKSTATE_H
#include "BaseState.h"
#include "MyGameWorld.h"
#include "ColorDefines.h"
#include "Message.h"

#define SEEK SphereSeekState::Instance()

class SphereSeekState : public BaseState
{
private:
	SphereSeekState(void);

public:
	
	~SphereSeekState(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static SphereSeekState* Instance();
};
#endif