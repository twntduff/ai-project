#pragma once
#include "Message.h"
#include "BaseState.h"

#define AACREATION ArmyAntCreationState::Instance()

class ArmyAntCreationState : public BaseState
{
private:
	ArmyAntCreationState(void);

public:
	~ArmyAntCreationState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static ArmyAntCreationState* Instance();
};

