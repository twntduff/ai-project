#pragma once
#include "Message.h"
#include "BaseState.h"

#define FLRELEASE FoodLargeReleaseState::Instance()

class FoodLargeReleaseState : public BaseState
{
private:
	FoodLargeReleaseState(void);

public:
	~FoodLargeReleaseState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static FoodLargeReleaseState* Instance();
};

