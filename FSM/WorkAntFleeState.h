#pragma once
#include "MyGameWorld.h"
#include "ColorDefines.h"
#include "Message.h"
#include "WorkerAnt.h"
#include "BaseState.h"

#define WAFLEE WorkAntFleeState::Instance()

class WorkAntFleeState : public BaseState
{
private:
	WorkAntFleeState(void);

public:
	~WorkAntFleeState(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static WorkAntFleeState* Instance();
};