#pragma once
#include "MyGameWorld.h"
#include "Message.h"
#include "WorkerAnt.h"
#include "BaseState.h"

#define WACREATION WorkAntCreationState::Instance()

class WorkAntCreationState : public BaseState
{
private:
	WorkAntCreationState(void);

public:
	~WorkAntCreationState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static WorkAntCreationState* Instance();
};

