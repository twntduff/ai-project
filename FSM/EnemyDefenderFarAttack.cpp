#include "EnemyDefenderFarAttack.h"

EnemyDefenderFarAttack::EnemyDefenderFarAttack(void)
{
}

EnemyDefenderFarAttack::~EnemyDefenderFarAttack(void)
{
}

void EnemyDefenderFarAttack::Enter(BaseEntity* entity)
{

}

void EnemyDefenderFarAttack::Execute(BaseEntity* entity)
{

}

void EnemyDefenderFarAttack::Exit(BaseEntity* entity)
{

}

EnemyDefenderFarAttack* EnemyDefenderFarAttack::Instance()
{
	static EnemyDefenderFarAttack* pInstance = new EnemyDefenderFarAttack();
	return pInstance;
}