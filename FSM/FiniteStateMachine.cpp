#include "FiniteStateMachine.h"


FiniteStateMachine::FiniteStateMachine(void)
{
}


FiniteStateMachine::~FiniteStateMachine(void)
{

}

FiniteStateMachine* FiniteStateMachine::Instance()
{
	FiniteStateMachine* pInstance = new FiniteStateMachine();
	return pInstance;
}

void FiniteStateMachine::Transition(BaseState* nextState, BaseEntity* entity)
{
	entity->GetCurrentState()->Exit(entity);
	nextState->Enter(entity);
	entity->SetCurrentState(nextState);
}

void FiniteStateMachine::Update(BaseEntity* entity)
{
	entity->GetCurrentState()->Execute(entity);
}