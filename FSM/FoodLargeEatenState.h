#pragma once
#include "Message.h"
#include"BaseState.h"

#define FLEATEN FoodLargeEatenState::Instance()

class FoodLargeEatenState : public BaseState
{
private:
	FoodLargeEatenState(void);

public:
	~FoodLargeEatenState(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static FoodLargeEatenState* Instance();
};

