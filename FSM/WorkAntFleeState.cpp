#include "WorkAntFleeState.h"
#include "SeekAlgo.h"
#include "Arrive.h"
#include "FleeAlgo.h"
#include "Patrol.h"
#include "MapManager.h"
#include "MovementManager.h"

WorkAntFleeState::WorkAntFleeState(void)
{
}


WorkAntFleeState::~WorkAntFleeState(void)
{
}

void WorkAntFleeState::Enter(BaseEntity* entity)
{
	SeekAlgo* tempAlgo = new SeekAlgo(entity->GetID());
	MOVE->AddAlgo(tempAlgo->GetID(), tempAlgo);
	entity->NotSent();
	entity->SetType(Flee);
	MAPMNGR->CalculateInfluence(EMNGR->GetEntityList());
	Node* tempNode = EMNGR->FindHomeBase(entity);
	entity->SetTraverseList(MAPMNGR->SetGoal(entity->GetCurrentNode(), tempNode, entity->GetTarget()));
	entity->SetTravNum(entity->GetTraverseList().size()-1);
	entity->SetTarget(entity->GetTraverseList()[entity->GetTravNum()]);
}

void WorkAntFleeState::Execute(BaseEntity* entity)
{
		if(entity->GetID() != entity->GetTargetId() && entity->GetPosition().Distance(entity->GetTarget()) <= 1 && entity->IsMessageSent() != true)
	{
		if(entity->GetTravNum() == 0)
		{
			Message* tempMessage1 = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Alive);
			tempMessage1->SendToManager();		
			Message* tempMessage2 = new Message(entity->GetTargetId(), entity->GetID(), timeGetTime(), 0,Eaten);
			tempMessage2->SendToManager();
			entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
			entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
			entity->IsSent();
			entity->FindZone(entity->GetPosition());
		}
		else
		{
			entity->FindZone(entity->GetPosition());
			entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
			entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
			entity->SetTravNum(entity->GetTravNum()-1);
			entity->SetTarget(entity->GetTraverseList()[entity->GetTravNum()]);
		}
	}
}

void WorkAntFleeState::Exit(BaseEntity* entity)
{

	MOVE->DeleteByID(entity->GetID());
}

WorkAntFleeState* WorkAntFleeState::Instance()
{
	static WorkAntFleeState* pInstance = new WorkAntFleeState();
	return pInstance;
}