#include "EnemyDefenderReleaseState.h"

EnemyDefenderReleaseState::EnemyDefenderReleaseState(void)
{
}

EnemyDefenderReleaseState::~EnemyDefenderReleaseState(void)
{
}

void EnemyDefenderReleaseState::Enter(BaseEntity* entity)
{
	entity->SetType(Dead);
	gMyGameWorld->GetSphere(entity->GetID())->SetColor(0);
}

void EnemyDefenderReleaseState::Execute(BaseEntity* entity)
{

}

void EnemyDefenderReleaseState::Exit(BaseEntity* entity)
{

}

EnemyDefenderReleaseState* EnemyDefenderReleaseState::Instance()
{
	static EnemyDefenderReleaseState* pInstance = new EnemyDefenderReleaseState();
	return pInstance;
}