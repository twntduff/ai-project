#include "ArmyAntReleaseState.h"


ArmyAntReleaseState::ArmyAntReleaseState(void)
{
}

ArmyAntReleaseState::~ArmyAntReleaseState(void)
{
}

void ArmyAntReleaseState::Enter(BaseEntity* entity)
{

}

void ArmyAntReleaseState::Execute(BaseEntity* entity)
{

}

void ArmyAntReleaseState::Exit(BaseEntity* entity)
{

}

ArmyAntReleaseState* ArmyAntReleaseState::Instance()
{
	static ArmyAntReleaseState* pInstance = new ArmyAntReleaseState();
	return pInstance;
}