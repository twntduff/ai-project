#pragma once
#include "BaseState.h"
#include "Message.h"

#define AAATTACK2 ArmyAntFarAttack::Instance()

class ArmyAntFarAttack : public BaseState
{
private:
	ArmyAntFarAttack(void);

public:
	~ArmyAntFarAttack(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static ArmyAntFarAttack* Instance();
};

