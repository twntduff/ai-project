#include "SphereCreationState.h"
#include "Algorithm.h"
#include "SeekAlgo.h"
#include "Arrive.h"
#include "FleeAlgo.h"
#include "Patrol.h"
#include "MovementManager.h"

SphereCreationState::SphereCreationState(void)
{
	
}

SphereCreationState::~SphereCreationState(void)
{

}

void SphereCreationState::Enter(BaseEntity* entity)
{
	/*real tempX = GetRandNumber(entity->GetPosition().GetX()-200, entity->GetPosition().GetX()+200);
	real tempY = GetRandNumber(entity->GetPosition().GetY()-200, entity->GetPosition().GetY()+200);
	real tempZ = GetRandNumber(entity->GetPosition().GetZ()-200, entity->GetPosition().GetZ()+200);
	entity->SetTarget(Vector3(tempX, tempY, tempZ));*/

	//Patrol* tempAlgo = new Patrol(entity->GetID());
	//MOVE->AddAlgo(tempAlgo->GetID(), tempAlgo);
	entity->NotSent();
	entity->SetType(Alive);
	gMyGameWorld->GetSphere(entity->GetID())->SetColor(GREEN);
	//gMyGameWorld->GetSphere(entity->GetID())->AddText("Creation");

	entity->SetHealth(100);
}

void SphereCreationState::Execute(BaseEntity* entity)
{
	//if(entity->GetID() != entity->GetTargetId() && entity->GetMass() != -1 && entity->GetPosition().Distance(EMNGR->GetEntityList().at(entity->GetTargetId())->GetPosition()) <= 100)
	//{
	//	entity->SetTarget(EMNGR->GetEntityList().at(entity->GetTargetId())->GetPosition());
	//}

	/*if(entity->GetMass() != -1 && entity->GetHealth() <= 0 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Dead);
		tempMessage->SendToManager();
		entity->IsSent();
	}*/
	if(entity->GetID() != entity->GetTargetId() && entity->GetMass() != -1 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Seek);
		tempMessage->SendToManager();
		entity->IsSent();
	}
	//else if(entity->GetID() != entity->GetTargetId() && entity->GetMass() != -1 && entity->GetHealth() <= 20 && entity->GetPosition().Distance(EMNGR->GetEntityList().at(entity->GetTargetId())->GetPosition()) <= 100 &&entity->IsMessageSent() != true)
	//{
	//	Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Flee);
	//	tempMessage->SendToManager();
	//	entity->IsSent();
	//}
}

void SphereCreationState::Exit(BaseEntity* entity)
{
	//if(entity->IsMessageSent() == true)
		//MOVE->DeleteByID(entity->GetID());
	//gMyGameWorld->ClearSphereText(entity->GetID());
}

SphereCreationState*  SphereCreationState::Instance()
{
	static SphereCreationState* pInstance = new SphereCreationState();
	return pInstance;
}

real SphereCreationState::GetRandNumber(int min, int max)
{
	return (rand()%(max-min)+min);
}