#include "SphereReleaseState.h"
#include "EntityManager.h"
#include "SeekAlgo.h"
#include "Arrive.h"
#include "FleeAlgo.h"
#include "Patrol.h"
#include "MovementManager.h"

SphereReleaseState::SphereReleaseState(void)
{

}

SphereReleaseState::~SphereReleaseState(void)
{

}

void SphereReleaseState::Enter(BaseEntity* entity)
{
	entity->NotSent();
	entity->SetType(Dead);
	gMyGameWorld->GetSphere(entity->GetID())->SetColor(RED);
	gMyGameWorld->GetSphere(entity->GetID())->AddText("Release");
}

void SphereReleaseState::Execute(BaseEntity* entity)
{
	if(entity->GetTargetId() != entity->GetID())
	{
		entity->SetTarget(EMNGR->GetEntityList().at(entity->GetTargetId())->GetPosition());
	}

	//gMyGameWorld->RemoveSphere(entity->GetID());
	//EMNGR->GetEntityList().erase(entity->GetID());
}

void SphereReleaseState::Exit(BaseEntity* entity)
{
	MOVE->DeleteByID(entity->GetID());
}

SphereReleaseState* SphereReleaseState::Instance()
{
	static SphereReleaseState* pInstance = new SphereReleaseState();
	return pInstance;
}