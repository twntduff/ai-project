#include "AntHillReleaseState.h"

AntHillReleaseState::AntHillReleaseState(void)
{
}

AntHillReleaseState::~AntHillReleaseState(void)
{
}

void AntHillReleaseState::Enter(BaseEntity* entity)
{

}

void AntHillReleaseState::Execute(BaseEntity* entity)
{

}

void AntHillReleaseState::Exit(BaseEntity* entity)
{

}

AntHillReleaseState* AntHillReleaseState::Instance()
{
	static AntHillReleaseState* pInstance = new AntHillReleaseState();
	return pInstance;
}