#ifndef FINITESTATEMACHINE_H
#define FINITESTATEMACHINE_H
#include "BaseState.h"
#include "Algorithm.h"
#include "MyGameWorld.h"

#define FSMGNR FiniteStateMachine::Instance()

class FiniteStateMachine
{
private:
	FiniteStateMachine(void);

public:
	~FiniteStateMachine(void);

	static FiniteStateMachine* Instance();

	//Method
	void Transition(BaseState* nextState, BaseEntity* entity);
	void Update(BaseEntity* entity);
};
#endif
