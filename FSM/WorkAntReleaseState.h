#pragma once
#include "MyGameWorld.h"
#include "ColorDefines.h"
#include "Message.h"
#include "WorkerAnt.h"
#include "BaseState.h"

#define WARELEASE WorkAntReleaseState::Instance()

class WorkAntReleaseState : public BaseState
{
private:
	WorkAntReleaseState(void);

public:
	~WorkAntReleaseState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static WorkAntReleaseState* Instance();
};

