#pragma once
#include "Message.h"
#include "BaseState.h"

#define FLCREATION FoodLargeCreationState::Instance()

class FoodLargeCreationState : public BaseState
{
private:
	FoodLargeCreationState(void);

public:
	~FoodLargeCreationState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static FoodLargeCreationState* Instance();
};

