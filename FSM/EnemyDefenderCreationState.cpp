#include "EnemyDefenderCreationState.h"
#include "MapManager.h"
#include "MovementManager.h"

EnemyDefenderCreationState::EnemyDefenderCreationState(void)
{
}

EnemyDefenderCreationState::~EnemyDefenderCreationState(void)
{
}

void EnemyDefenderCreationState::Enter(BaseEntity* entity)
{
	entity->NotSent();
	entity->SetType(Alive);
	entity->SetHealth(100);
	entity->FindZone(entity->GetPosition());
	entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
	entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
}

void EnemyDefenderCreationState::Execute(BaseEntity* entity)
{
	if(entity->GetHealth() <= 0)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(),timeGetTime(), 0, Dead);
		tempMessage->SendToManager();
		entity->IsSent();
	}
}

void EnemyDefenderCreationState::Exit(BaseEntity* entity)
{

}

EnemyDefenderCreationState* EnemyDefenderCreationState::Instance()
{
	static EnemyDefenderCreationState* pInstance = new EnemyDefenderCreationState();
	return pInstance;
}