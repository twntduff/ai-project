#pragma once
#include "MyGameWorld.h"
#include "ColorDefines.h"
#include "Message.h"
#include "WorkerAnt.h"
#include "BaseState.h"

#define WASEEK WorkAntSeekState::Instance()

class WorkAntSeekState : public BaseState
{
private:
	WorkAntSeekState(void);

public:
	~WorkAntSeekState(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static WorkAntSeekState* Instance();
};

