#include "AntHillCreationState.h"
#include "MapManager.h"
#include "MovementManager.h"

AntHillCreationState::AntHillCreationState(void)
{
}

AntHillCreationState::~AntHillCreationState(void)
{
}

void AntHillCreationState::Enter(BaseEntity* entity)
{
	entity->NotSent();
	entity->SetType(Alive);
	entity->FindZone(entity->GetPosition());
	entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
	entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
}

void AntHillCreationState::Execute(BaseEntity* entity)
{

}

void AntHillCreationState::Exit(BaseEntity* entity)
{

}

AntHillCreationState* AntHillCreationState::Instance()
{
	static AntHillCreationState* pInstance = new AntHillCreationState();
	return pInstance;
}