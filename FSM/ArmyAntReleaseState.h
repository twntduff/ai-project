#pragma once
#include "BaseState.h"
#include "Message.h"

#define AARELEASE ArmyAntReleaseState::Instance()

class ArmyAntReleaseState : public BaseState
{
private:
	ArmyAntReleaseState(void);

public:
	~ArmyAntReleaseState(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static ArmyAntReleaseState* Instance();
};

