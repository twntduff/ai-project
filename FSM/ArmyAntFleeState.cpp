#include "ArmyAntFleeState.h"


ArmyAntFleeState::ArmyAntFleeState(void)
{
}

ArmyAntFleeState::~ArmyAntFleeState(void)
{
}

void ArmyAntFleeState::Enter(BaseEntity* entity)
{

}

void ArmyAntFleeState::Execute(BaseEntity* entity)
{

}

void ArmyAntFleeState::Exit(BaseEntity* entity)
{

}

ArmyAntFleeState* ArmyAntFleeState::Instance()
{
	static ArmyAntFleeState* pInstance = new ArmyAntFleeState();
	return pInstance;
}