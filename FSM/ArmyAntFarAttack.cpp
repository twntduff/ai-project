#include "ArmyAntFarAttack.h"

ArmyAntFarAttack::ArmyAntFarAttack(void)
{
}

ArmyAntFarAttack::~ArmyAntFarAttack(void)
{
}

void ArmyAntFarAttack::Enter(BaseEntity* entity)
{

}

void ArmyAntFarAttack::Execute(BaseEntity* entity)
{

}

void ArmyAntFarAttack::Exit(BaseEntity* entity)
{

}

ArmyAntFarAttack* ArmyAntFarAttack::Instance()
{
	static ArmyAntFarAttack* pInstance = new ArmyAntFarAttack();
	return pInstance;
}