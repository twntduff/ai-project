#pragma once
#include "BaseState.h"
#include "Message.h"

#define EDATTACK2 EnemyDefenderFarAttack::Instance()

class EnemyDefenderFarAttack : public BaseState
{
private:
	EnemyDefenderFarAttack(void);

public:
	~EnemyDefenderFarAttack(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static EnemyDefenderFarAttack* Instance();
};

