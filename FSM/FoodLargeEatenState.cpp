#include "FoodLargeEatenState.h"

FoodLargeEatenState::FoodLargeEatenState(void)
{
}

FoodLargeEatenState::~FoodLargeEatenState(void)
{
}

void FoodLargeEatenState::Enter(BaseEntity* entity)
{

}

void FoodLargeEatenState::Execute(BaseEntity* entity)
{

}

void FoodLargeEatenState::Exit(BaseEntity* entity)
{

}

FoodLargeEatenState* FoodLargeEatenState::Instance()
{
	static FoodLargeEatenState* pInstance = new FoodLargeEatenState();
	return pInstance;
}