#pragma once
#include "BaseState.h"
#include "Message.h"

#define AHCREATION AntHillCreationState::Instance()

class AntHillCreationState : public BaseState
{
private:
	AntHillCreationState(void);

public:
	~AntHillCreationState(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static AntHillCreationState* Instance();
};

