#pragma once
#include "BaseState.h"
#include "Message.h"

#define EDATTACK1 EnemyDefenderCloseAttack::Instance()

class EnemyDefenderCloseAttack : public BaseState
{
private:
	EnemyDefenderCloseAttack(void);

public:
	~EnemyDefenderCloseAttack(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static EnemyDefenderCloseAttack* Instance();
};

