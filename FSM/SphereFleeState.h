#ifndef SPHEREFLEESTATE_H
#define SPHEREFLEESTATE_H
#include "BaseState.h"
#include "MyGameWorld.h"
#include "ColorDefines.h"
#include "AISphereEntity.h"
#include "Message.h"

#define FLEE SphereFleeState::Instance()

class SphereFleeState : public BaseState
{
private:
	SphereFleeState(void);

public:
	~SphereFleeState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static SphereFleeState* Instance();
};
#endif