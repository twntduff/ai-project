#include "EnemyDefenderAttack1.h"

EnemyDefenderCloseAttack::EnemyDefenderCloseAttack(void)
{
}

EnemyDefenderCloseAttack::~EnemyDefenderCloseAttack(void)
{
}

void EnemyDefenderCloseAttack::Enter(BaseEntity* entity)
{

}

void EnemyDefenderCloseAttack::Execute(BaseEntity* entity)
{

}

void EnemyDefenderCloseAttack::Exit(BaseEntity* entity)
{

}

EnemyDefenderCloseAttack* EnemyDefenderCloseAttack::Instance()
{
	static EnemyDefenderCloseAttack* pInstance = new EnemyDefenderCloseAttack();
	return pInstance;
}