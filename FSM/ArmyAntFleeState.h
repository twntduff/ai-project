#pragma once
#include "BaseState.h"
#include "Message.h"

#define AAFLEE ArmyAntFleeState::Instance()

class ArmyAntFleeState : public BaseState
{
private:
	ArmyAntFleeState(void);

public:
	~ArmyAntFleeState(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static ArmyAntFleeState* Instance();
};

