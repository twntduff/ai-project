#pragma once
#include "BaseState.h"
#include "Message.h"

#define AAATTACK1 ArmyAntCloseAttackState::Instance()

class ArmyAntCloseAttackState : public BaseState
{
private:
	ArmyAntCloseAttackState(void);

public:
	~ArmyAntCloseAttackState(void);

	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static ArmyAntCloseAttackState* Instance();
};

