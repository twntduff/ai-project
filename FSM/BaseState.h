#ifndef BASESTATE_H
#define BASESTATE_H
#include "BaseEntity.h"
#include "EntityManager.h"

class BaseState
{
public:
	BaseState(void);
	~BaseState(void);
	virtual void Enter(BaseEntity* entity) = 0;
	virtual void Execute(BaseEntity* entity) = 0;
	virtual void Exit(BaseEntity* entity) = 0;
};
#endif