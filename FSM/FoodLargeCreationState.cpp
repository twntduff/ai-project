#include "FoodLargeCreationState.h"
#include "MapManager.h"

FoodLargeCreationState::FoodLargeCreationState(void)
{
}

FoodLargeCreationState::~FoodLargeCreationState(void)
{
}

void FoodLargeCreationState::Enter(BaseEntity* entity)
{
	entity->SetType(Alive);
	entity->NotSent();
	entity->SetHealth(100);
	entity->FindZone(entity->GetPosition());
	entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
	entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
}

void FoodLargeCreationState::Execute(BaseEntity* entity)
{
	if(entity->GetHealth() <= 0)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(),timeGetTime(), 0, Dead);
		tempMessage->SendToManager();
		entity->IsSent();
	}
}

void FoodLargeCreationState::Exit(BaseEntity* entity)
{

}

FoodLargeCreationState* FoodLargeCreationState::Instance()
{
	static FoodLargeCreationState* pInstance = new FoodLargeCreationState();
	return pInstance;
}