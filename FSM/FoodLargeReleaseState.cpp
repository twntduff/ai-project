#include "FoodLargeReleaseState.h"


FoodLargeReleaseState::FoodLargeReleaseState(void)
{
}


FoodLargeReleaseState::~FoodLargeReleaseState(void)
{
}

void FoodLargeReleaseState::Enter(BaseEntity* entity)
{
	entity->SetType(Dead);
	gMyGameWorld->GetSquare(entity->GetID()+1)->SetColor(0);
	//EMNGR->ChangeFoodSorce(entity->GetID());
	//gMyGameWorld->RemoveSphere(entity->GetID());
	//EMNGR->DeleteEntityByID(entity->GetID());
}

void FoodLargeReleaseState::Execute(BaseEntity* entity)
{

}

void FoodLargeReleaseState::Exit(BaseEntity* entity)
{

}

FoodLargeReleaseState* FoodLargeReleaseState::Instance()
{
	static FoodLargeReleaseState* pInstance = new FoodLargeReleaseState();
	return pInstance;
}