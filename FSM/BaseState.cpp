#include "BaseState.h"

BaseState::BaseState(void)
{

}

BaseState::~BaseState(void)
{

}

void BaseState::Enter(BaseEntity* entity)
{

}

void BaseState::Execute(BaseEntity* entity)
{

}

void BaseState::Exit(BaseEntity* entity)
{

}