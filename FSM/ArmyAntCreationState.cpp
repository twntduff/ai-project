#include "ArmyAntCreationState.h"
#include "SeekAlgo.h"
#include "MapManager.h"
#include "MovementManager.h"

ArmyAntCreationState::ArmyAntCreationState(void)
{
}

ArmyAntCreationState::~ArmyAntCreationState(void)
{
}

void ArmyAntCreationState::Enter(BaseEntity* entity)
{
	entity->SetType(Alive);
	entity->NotSent();
	entity->SetHealth(100);
	entity->FindZone(entity->GetPosition());

	SeekAlgo* tempAlgo = new SeekAlgo(entity->GetID());
	MOVE->AddAlgo(tempAlgo->GetID(),tempAlgo);

	Node* tempNode = MAPMNGR->FindClosestNode(entity);

	entity->SetTarget(tempNode->GetPosition());
	entity->SetCurrentNode(tempNode);
	entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
	entity->SetTraverseList(MAPMNGR->SetGoal(tempNode, tempNode, Vector3(0,0,0)));
}

void ArmyAntCreationState::Execute(BaseEntity* entity)
{
	if(entity->GetPosition().Distance(entity->GetCurrentNode()->GetPosition()) <= 1 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Attack1);
		tempMessage->SendToManager();
		entity->IsSent();
	}
}

void ArmyAntCreationState::Exit(BaseEntity* entity)
{
	MOVE->DeleteByID(entity->GetID());
}

ArmyAntCreationState* ArmyAntCreationState::Instance()
{
	static ArmyAntCreationState* pInstance = new ArmyAntCreationState();
	return pInstance;
}