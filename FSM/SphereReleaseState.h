#ifndef SPHERERELEASESTATE_H
#define SPHERERELEASESTATE_H
#include "BaseState.h"
#include "MyGameWorld.h"
#include "ColorDefines.h"

#define RELEASE SphereReleaseState::Instance()

class SphereReleaseState : public BaseState
{
private:
	SphereReleaseState(void);

public:
	~SphereReleaseState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static SphereReleaseState* Instance();
};
#endif