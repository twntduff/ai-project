#pragma once
#include "BaseState.h"
#include "Message.h"

#define AHRELEASE AntHillReleaseState::Instance()

class AntHillReleaseState : public BaseState
{
private:
	AntHillReleaseState(void);

public:
	~AntHillReleaseState(void);

	//Method
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);
	static AntHillReleaseState* Instance();
};

