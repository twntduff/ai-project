#ifndef SPHERECREATIONSTATE_H
#define SPHERECREATIONSTATE_H
#include "MyGameWorld.h"
#include "ColorDefines.h"
#include "Message.h"
#include "AISphereEntity.h"
#include "BaseState.h"

#define CREATION SphereCreationState::Instance()

class SphereCreationState : public BaseState
{
private:
	SphereCreationState(void);

public:
	~SphereCreationState(void);
	void Enter(BaseEntity* entity);
	void Execute(BaseEntity* entity);
	void Exit(BaseEntity* entity);

	//Method
	static SphereCreationState* Instance();
	real GetRandNumber(int min, int max);
};
#endif