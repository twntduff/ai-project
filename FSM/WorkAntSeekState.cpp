#include "WorkAntSeekState.h"
#include "SeekAlgo.h"
#include "Arrive.h"
#include "FleeAlgo.h"
#include "Patrol.h"
#include "MapManager.h"
#include "MovementManager.h"

WorkAntSeekState::WorkAntSeekState(void)
{
}


WorkAntSeekState::~WorkAntSeekState(void)
{
}

void WorkAntSeekState::Enter(BaseEntity* entity)
{
	SeekAlgo* tempAlgo = new SeekAlgo(entity->GetID());
	MOVE->AddAlgo(tempAlgo->GetID(), tempAlgo);
	entity->NotSent();
	entity->SetType(Seek);
	MAPMNGR->CalculateInfluence(EMNGR->GetEntityList());
	Node* tempNode = EMNGR->FindClosestFoodSource(entity);
	entity->SetTraverseList(MAPMNGR->SetGoal(entity->GetCurrentNode(), tempNode, entity->GetTarget()));
	entity->SetTravNum(entity->GetTraverseList().size()-1);
	entity->SetTarget(entity->GetTraverseList()[entity->GetTravNum()]);
}

void WorkAntSeekState::Execute(BaseEntity* entity)
{
	if(entity->GetID() != entity->GetTargetId() && entity->GetPosition().Distance(entity->GetTarget()) <= 1 && entity->IsMessageSent() != true)
	{
		if(EMNGR->GetEntityList()[entity->GetTargetId()]->GetType() == Dead)
		{
			MAPMNGR->CalculateInfluence(EMNGR->GetEntityList());
			Node* tempNode = EMNGR->FindClosestFoodSource(entity);
			entity->SetTraverseList(MAPMNGR->SetGoal(entity->GetCurrentNode(), tempNode, entity->GetTarget()));
			entity->SetTravNum(entity->GetTraverseList().size()-1);
			entity->SetTarget(entity->GetTraverseList()[entity->GetTravNum()]);
		}
		else if(entity->GetTravNum() == 0)
		{
			Message* tempMessage1 = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Flee);
			tempMessage1->SendToManager();
			if(EMNGR->GetEntityList()[entity->GetTargetId()]->GetType() != Dead)
			{
				Message* tempMessage2 = new Message(entity->GetTargetId(), entity->GetID(), timeGetTime(), 0,Eaten);
				tempMessage2->SendToManager();
			}
			entity->FindZone(entity->GetPosition());
			entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
			entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
			entity->IsSent();
		}
		else
		{
			entity->FindZone(entity->GetPosition());
			entity->SetCurrentNode(MAPMNGR->FindClosestNode(entity));
			entity->SetCurrentNodeId(entity->GetCurrentNode()->GetID());
			entity->SetTravNum(entity->GetTravNum()-1);
			entity->SetTarget(entity->GetTraverseList()[entity->GetTravNum()]);
		}
	}
}

void WorkAntSeekState::Exit(BaseEntity* entity)
{
	MOVE->DeleteByID(entity->GetID());
}

WorkAntSeekState* WorkAntSeekState::Instance()
{
	static WorkAntSeekState* pInstance = new WorkAntSeekState();
	return pInstance;
}