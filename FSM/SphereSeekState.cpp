#include "SphereSeekState.h"
#include "Algorithm.h"
#include "SeekAlgo.h"
#include "Arrive.h"
#include "FleeAlgo.h"
#include "Patrol.h"
#include "MovementManager.h"

SphereSeekState::SphereSeekState(void)
{

}

SphereSeekState::~SphereSeekState(void)
{

}

void SphereSeekState::Enter(BaseEntity* entity)
{
	entity->SetTravNum(entity->GetTraverseList().size()-1);
	SeekAlgo* tempAlgo = new SeekAlgo(entity->GetID());
	MOVE->AddAlgo(tempAlgo->GetID(), tempAlgo);
	entity->NotSent();
	entity->SetType(Seek);
	gMyGameWorld->GetSphere(entity->GetID())->SetColor(BLUE);
	gMyGameWorld->GetSphere(entity->GetID())->AddText("Seek");
}

void SphereSeekState::Execute(BaseEntity* entity)
{
	if(entity->GetID() != entity->GetTargetId() && entity->GetPosition().Distance(entity->GetTarget()) <= 1 && entity->IsMessageSent() != true)
	{
		if(entity->GetTravNum() == 0)
			entity->IsSent();
		else
			entity->SetTravNum(entity->GetTravNum()-1);
			entity->SetTarget(entity->GetTraverseList()[entity->GetTravNum()]);
	}

	if(entity->GetHealth() <= 0 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Dead);
		tempMessage->SendToManager();
		entity->IsSent();
	}
	else if(entity->GetHealth() <= 20 && entity->IsMessageSent() != true)
	{
		Message* tempMessage = new Message(entity->GetID(), entity->GetID(), timeGetTime(), 0,Flee);
		tempMessage->SendToManager();
		entity->IsSent();
	}
}

void SphereSeekState::Exit(BaseEntity* entity)
{
	MOVE->DeleteByID(entity->GetID());
	gMyGameWorld->ClearSphereText(entity->GetID());
}

SphereSeekState* SphereSeekState::Instance()
{
	static SphereSeekState* pInstance = new SphereSeekState();
	return pInstance;
}